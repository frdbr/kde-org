---
title: "KDE's Goals"
summary: KDE's Goals for the Next Couple of Years.
header_image_link: KGoals-logo.png
header_image_alt: KDE Goals logo
scssFiles:
 - /scss/goals.scss
---

<div class="text-center">
    <img src="https://kde.org/content/goals/KGoals-logo.png" style="width:65%;height:65%;margin:70px 0 70px 0" alt="KDE Goals logo" />
</div>

<p>KDE is a huge and diverse community working on hundreds of projects. To guide our work, we have selected 3 overarching goals to focus on over the next couple of years.</p>

<a class="button ms-1 me-1" href="https://go.kde.org/matrix/#/#goals:kde.org">General Chat Room</a>

<div class="section">
  <h2>Streamlined Application Development Experience</h2>

  <p>This goal focuses on improving the application development process. By making it easier for developers to create applications, KDE hopes to attract more contributors and deliver better software for both first-party and third-party applications. A notable task within this goal is enhancing the experience of building KDE apps with languages beyond C++, such as Rust or Python.</p>

  <p class="info">Champions: Nicolas Fella and Nate Graham</p>

  <a class="button ms-1 me-1" href="https://invent.kde.org/teams/goals/streamlined-application-development-experience">Workboard</a>
  <a class="button ms-1 me-1" href="https://go.kde.org/matrix/#/#kde-streamlined-app-dev:kde.org">Chat</a>
  <a class="button mb-3 ms-1 me-1" href="https://phabricator.kde.org/T17396">Proposal</a>
</div>

<div class="section">
  <h2>We care about your Input</h2>

  <p>KDE has a diverse users base with unique input needs: artists using complex monitor and drawing tablet setups; gamers with controllers, fancy mice, and handhelds; users requiring accessibility features or using a language optimally types with complex input methods; students with laptops, 2-in-1s, and tablets — and more! While KDE has made significant progress in supporting these diverse sources of input input over the years, there are still gaps to be addressed. This goal aims to close those gaps and deliver a truly seamless input experience for everyone.</p>

  <p class="info">Champions: Gernot Schiller, Jakob Petsovits and Joshua Goins</p>

  <a class="button ms-1 me-1" href="https://invent.kde.org/teams/goals/we-care-about-your-input">Workboard</a>
  <a class="button ms-1 me-1" href="https://go.kde.org/matrix/#/#kde-input:kde.org">Chat</a>
  <a class="button mb-3 ms-1 me-1" href="https://phabricator.kde.org/T17433">Proposal</a>
</div>

<div class="section">
  <h2>KDE Needs You! 🫵</h2>

  <p>KDE’s growth depends on new contributors, but a lack of fresh involvement in key projects like Plasma, Kdenlive, Krita, GCompris, and others is a concern. This goal focuses on formalizing and enhancing recruitment processes, not just for individuals but also for institutions. Ensuring that bringing in new talent becomes a continuous and community-wide priority, vital for KDE's long-term sustainability.</p>

  <p class="info">Champions: Aniqa Khokhar, Johnny Jazeix and Paul Brown</p>

  <a class="button ms-1 me-1" href="https://invent.kde.org/teams/goals/contributor-onboarding">Workboard</a>
  <a class="button mb-3 ms-1 me-1" href="https://phabricator.kde.org/T17439">Proposal</a>
</div>
