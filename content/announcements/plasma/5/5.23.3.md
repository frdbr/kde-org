---
date: 2021-11-09
changelog: 5.23.2-5.23.3
layout: plasma
asBugfix: true
draft: false
---

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.mp4" poster="https://kde.org/announcements/plasma/5/5.23.0/Plasma25AE_b.png" >}}

+ Keyboardlayout: Fix missing Esperanto flag icon in the menu. [Commit.](http://commits.kde.org/plasma-desktop/50a42b490ef595c3872c2c60b57cf7240c2a7b46) Fixes bug [#445077](https://bugs.kde.org/445077)
+ Plasma Networkmanager: OpenVPN: fix logic in auth dialog. [Commit.](http://commits.kde.org/plasma-nm/48fad4ac77520d673414ef957e6dedc4d151eb73) Fixes bug [#444882](https://bugs.kde.org/444882)
+ On FreeBSD do not try to execute powerdevil.backlighthelper.syspath action. [Commit.](http://commits.kde.org/powerdevil/747f684fcb65206d23091b3134dc9256598298e3)
