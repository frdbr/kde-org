---
date: 2024-02-15
appCount: 120
image: true
layout: gear
---
+ knavalbattle: Fix test for placing a ship vertically ([Commit](http://commits.kde.org/knavalbattle/c3a96f90fbfd4e9d705c3bbc09c436491b559716))
+ konsole: Show wallpaper on non-translucent top-levels ([Commit](http://commits.kde.org/konsole/80e6492114afe063d700309600d35aaa3ebceb9c), fixes bug [#477800](https://bugs.kde.org/477800))
+ neochat: Fix saving images ([Commit](http://commits.kde.org/neochat/d26a6916478b9eb76c9d09a675c74ce6a04df3cd), fixes bug [#479053](https://bugs.kde.org/479053))
