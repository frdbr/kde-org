---
date: 2024-07-04
appCount: 180
layout: gear
---
+ kdepim-runtime:  Fix a memory leak in the EWS resource ([Commit](http://commits.kde.org/kdepim-runtime/6173a24fb1aa4a13978185184b74c96934c2aa50), fixes bug [#486861](https://bugs.kde.org/486861))
+ kio-gdrive: Fix "This file does not exist" after clicking on a folder ([Commit](http://commits.kde.org/kio-gdrive/027c25b340e0405a0a3e0c53bf4d56454f99ca28), fixes bug [#487021](https://bugs.kde.org/487021))
+ partitionmanager: Fix a crash caused by clicking the remove mount point button ([Commit](http://commits.kde.org/partitionmanager/ee14890037bd77cf87e6c0da2649bd9cab473d55), fixes bug [#432103](https://bugs.kde.org/432103))
