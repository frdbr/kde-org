---
aliases:
- /announcements/plasma-5.5.0-5.5.1-changelog
hidden: true
plasma: true
title: Plasma 5.5.1 complete changelog
type: fulllog
version: 5.5.1
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Cleanup shadowhelper's widget registration logic. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e78f76fbcb512f682f6261870996aad36273a1a0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356625'>#356625</a>

### <a name='breeze-gtk' href='http://quickgit.kde.org/?p=breeze-gtk.git'>Breeze GTK</a>

- Relicence to LGPL 2.1+ for consistency with the rest of the repo. <a href='http://quickgit.kde.org/?p=breeze-gtk.git&amp;a=commit&amp;h=6201e845bd100ffbe5f8d91e99924b5611db7b14'>Commit.</a>

### <a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a>

- Update reviewboardrc. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f0bb69be884417aa06b0e84cb66fd21039e343f5'>Commit.</a>
- Prevent notifier from crashing when process was not initialized yet. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=86b6b88dd04d68bc92942c9460335e3efb171fc7'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126341'>#126341</a>
- App notifier is crashing right now. build a test around it. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=149a417fba3a754c4520d87c76b367e8e6af25c3'>Commit.</a>
- Update notifier metadata.desktop. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=fbb13a4b50e2147263265f47d9549db7b58f9cb8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356568'>#356568</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- [Color Picker] Fix grabbing color in multi screen environment. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=7c714d1756b6760350c2fdc09b661688d7f0473a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356464'>#356464</a>
- [User Switcher plasmoid] Encapsulate delegate highlight :). <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=142df9fcb4fb26148fe8da8cc004fe5cfaf10625'>Commit.</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- [backends/fbdev] Support framebuffers with different color depths. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3247382752071f77b78fb714572f7d31f148e47a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126262'>#126262</a>. Fixes bug <a href='https://bugs.kde.org/355569'>#355569</a>
- Unblock signals in child processes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=14b9046ad2ae7d4b9e3ffda996b2112fae3690c4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356580'>#356580</a>. Code review <a href='https://git.reviewboard.kde.org/r/126361'>#126361</a>
- [wayland] Forward error channel of launched processes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2d6fe2cb05449f3ee90d400a647f03523348565d'>Commit.</a>
- Ensure to cancel animation on p-re-invocation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=86f2a71b9a7aadcaf241b4e599e8364756798057'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356381'>#356381</a>. Code review <a href='https://git.reviewboard.kde.org/r/126277'>#126277</a>
- Avoid pointless kbd grab on moveresize. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fab806df9297d901625edf03eccd8f4a77875ac9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126266'>#126266</a>

### <a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a>

- Cleanup shadowhelper's widget registration logic. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=0fc37dcb5ba7b5dbbadfd9416ffc44d48a9b5e4b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356625'>#356625</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- [CompactApplet] Hack to force focus on expanded representation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=cb583af2b39317f3500306ce6d0644136d170d88'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126292'>#126292</a>
- Don't size placeholder for handle when handle is not shown (e.g. on drag from frame deadspace). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=556fd51a5c1b0753dfe9389b72d5b3caeb2cb3ab'>Commit.</a>
- Use gridUnit. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=60b7277532dbeffb5e87b1f5ec2fd415eb114d07'>Commit.</a>
- Fix cell size not scaling on hidpi systems. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2efeb8566b75d9ca2e65f25ef0bd2b7e787c3b1f'>Commit.</a>
- Fix applet handle not scaling on hidpi systems. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9e8f7c9877c5c5d9b3109462ccc7e515a121d7cc'>Commit.</a>
- [Keyboard KCM] Use getShortText for OSD as well. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=777df8e073d6914830ec81fe2285fe85511e1217'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126333'>#126333</a>
- Fix build. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3cef547420190ec00668affa51c440960a723c7c'>Commit.</a>
- Make sure press-and-hold isn't triggered during DND inside of (and never leaving) Folder View. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=02f72c80b7a12b3826f4b309aa9d4604632358b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356582'>#356582</a>
- KCM Removable Devices: Fix enabling ui elements when changing global enabled setting. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c582f9d8e5f0d99306c7a57fa28f7b347bd4f3e6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126337'>#126337</a>
- Make drag pixmap scale and fix hotspot. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a6b9fb3d99a1873d1a0fc7e57f906f8d9d988f44'>Commit.</a>
- [Kickoff] Go to Favorites when adding one. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2bbb69ebb0b830c09aafc4a3b089d919f45b5c8e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126280'>#126280</a>
- Map to source row. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=89637a3adf52a0a563e62d6b6e4a1c63ed8b4e93'>Commit.</a>
- Filter out invalid apps. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=736115b19d69911caffb82bdb6f6883b4df0612d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353182'>#353182</a>
- Fix migration of multiple favorites. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=97ced66da372737ae5d36d86c9bc0c6fe684d842'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356398'>#356398</a>
- Kick in small screen mode on 1600x900 (née vertically small screens) as well. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a1c59241f7ecc40d73e695c6c289a7acc9c3b7ce'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354283'>#354283</a>
- Don't access the wrong applet. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=981010ccde7b2b66112134017f4948fd429bd155'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355628'>#355628</a>
- Improve preferred geometry calculation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1e3b92679642c861751cf3cf22cbe46eab568583'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356287'>#356287</a>
- Make Key_Escape abort search first again. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=30865c8c1a4e495bbf3b12a4e42e3781e04e6f48'>Commit.</a>
- Rewrite Appdash key handling. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=78b4096044fd8e125e5170f00eb5eb1944a2e4a4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352416'>#352416</a>. Fixes bug <a href='https://bugs.kde.org/354754'>#354754</a>. Fixes bug <a href='https://bugs.kde.org/356269'>#356269</a>
- Fix 'Show Original File'. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f687564e5895f8aabb4f33ad856cae365615a39c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350517'>#350517</a>
- Fix Ark D-Bus invocation for extract drops. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4018e27adfc0f146dbeabe2784010aaa1ac3fb2f'>Commit.</a>

### <a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a>

- Fix openconnect dialog. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=28c4b2e1b868427201cecb6d01cdb45ebfd157a0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356587'>#356587</a>. Fixes bug <a href='https://bugs.kde.org/356622'>#356622</a>
- Set minimum size of popup dialog. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=c0c3c03139c787ed97e0bd7edd2cace36697af20'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356342'>#356342</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- [notifications] Remove some useless debug output. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=40554031d46891d9ab0e0faaab4a89b27c36896c'>Commit.</a>
- Mitigate failed icon grabbing in xembed-sni-proxy. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=41df1bdb8b478eb27fb424d201c075c76ec0ed5a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355684'>#355684</a>. Code review <a href='https://git.reviewboard.kde.org/r/126336'>#126336</a>
- [System Tray] Explicitly forward key events to expanded task. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=46310dcb50bed485550510ed5f70a623041d84ab'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126293'>#126293</a>
- [Clipboard Plasmoid] Fix import to use StandardKey. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9b2396770dd103c6b549763a7f13a896e6af19d8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126294'>#126294</a>
- If the user runs failsafe mode, use software rendering in QtQuick. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b5ebbb01b20509571eb8dab03ed7b49891d9bad4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126205'>#126205</a>
- Move shutdown scripts into ksmserver cleanup. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1dc7f11692a4096b9815ae24f7be9cec10f8f7a5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356190'>#356190</a>. Code review <a href='https://git.reviewboard.kde.org/r/126268'>#126268</a>
- [notifications] Fix default notification position setting. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5d56df0d53851a8cd61344d356bcb552220dd3ed'>Commit.</a> See bug <a href='https://bugs.kde.org/356461'>#356461</a>
- Make "comment" section of the timezones configuration searchable. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6381dbd9b7a0f33dd024a98ef18a5143ba929b0e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354238'>#354238</a>. Code review <a href='https://git.reviewboard.kde.org/r/126302'>#126302</a>
- [notifications] Move reading globalConfig() from ctor to init(). <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5c5bbc3f8c4297cfd6862072aa33a45d947fa41b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356419'>#356419</a>
- [notifications] Check if the popup is visible before updating its geometry. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2b59ccd531dcdb7d86577cee1d987f95a9db9bf0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356402'>#356402</a>

### <a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a>

- [XRandRBrightness] Cache XCB connection in variable. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=284ca7af172ea94a3fb8a631b220db50f1da7b91'>Commit.</a>
- [XRandRBrightness] Don't call for xrandr if it's not available. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=dde0ecce02259a26655737e6674950e2e303bc6f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126146'>#126146</a>. Fixes bug <a href='https://bugs.kde.org/352462'>#352462</a>