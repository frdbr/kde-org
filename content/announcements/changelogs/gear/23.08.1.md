---
aliases:
- ../../fulllog_releases-23.08.1
title: KDE Gear 23.08.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Use correct identity when sending iTIP counter-proposal. [Commit](http://commits.kde.org/akonadi-calendar/89a05bc3db6cd14172fe4222a3f33b1ab18a80e7). Fixes bug [#458524](https://bugs.kde.org/458524).
{{< /details >}}
{{< details id="akregator" title="akregator" link="https://commits.kde.org/akregator" >}}
+ Add project_license to appstream metadata. [Commit](http://commits.kde.org/akregator/8d4078b3a23b4baad90489d4bb81f5c155f26a52).
{{< /details >}}
{{< details id="calindori" title="calindori" link="https://commits.kde.org/calindori" >}}
+ Add Keywords to org.kde.calindori.desktop. [Commit](http://commits.kde.org/calindori/081bc28a8ba60eb59c7273b411984fdc77529a8f).
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Copy Location: Make sure to export path with native separators. [Commit](http://commits.kde.org/dolphin/967189bc8361942696c288837db27f4eef1a606a).
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Agenda: fix timezone being lost when moving event by drag. [Commit](http://commits.kde.org/eventviews/def2b7f4cc507bcf8f64540e77dd736ebb751e7d). See bug [#448703](https://bugs.kde.org/448703).
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Fix navigation with side mouse buttons. [Commit](http://commits.kde.org/gwenview/9422e2129c5059df232849b4638f2dd8527a66d3).
+ Stop overriding settings menu. [Commit](http://commits.kde.org/gwenview/3f0de66d7a703a5ba2534fb8dc993ddb06faae7e). Fixes bug [#473672](https://bugs.kde.org/473672).
+ Don't mark two English strings as untranslated. [Commit](http://commits.kde.org/gwenview/ba9196da9ab7a5dc8f8e7322961a213ac823195d). Fixes bug [#473637](https://bugs.kde.org/473637).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Fix editing bus and train elements without an associated ticket object. [Commit](http://commits.kde.org/itinerary/8ac9954a07f7d18b86b5927c74b3172a45a1d78f). Fixes bug [#474234](https://bugs.kde.org/474234).
+ Manually manage permissions in the Android manifest. [Commit](http://commits.kde.org/itinerary/ea46f6edae059d2f6a83bb8901d9142349a1008a).
+ Correctly size constrain Apple Wallet generic pass thumbnail image. [Commit](http://commits.kde.org/itinerary/975d98a2cfb4c5c7c6da2fee1d04dbc6dac3233e).
+ Fix progress computation on the initial journey stopover. [Commit](http://commits.kde.org/itinerary/fa7f2b22542e9960f7e1409e9fd068ee5dbc6fdf).
+ Unbreak Flatpak build after the runtime removed ZXing. [Commit](http://commits.kde.org/itinerary/60c79594e4a77fa8338c20f1331c9d42cd32ebc8).
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Remove unused variables. [Commit](http://commits.kde.org/kalarm/db7d613b89746bca9c8e8ec9a40195172815a226).
+ Bug 473792: Don't warn about not providing high accuracy alarms if alarm >10 minutes from now. [Commit](http://commits.kde.org/kalarm/65031e6e03c802a9e3669ee34c95e6d919cfff8e).
{{< /details >}}
{{< details id="kasts" title="kasts" link="https://commits.kde.org/kasts" >}}
+ Bump KF minimum version to 5.105.0. [Commit](http://commits.kde.org/kasts/b9fc8915a7e15caa0316e92dfbb55433ed7d78c5). See bug [#473648](https://bugs.kde.org/473648).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Lsp: Fix crash on clicking "Close dynamic reference tabs". [Commit](http://commits.kde.org/kate/f938e6932821d56a1ee2636e73844e541e5a70f1). Fixes bug [#474216](https://bugs.kde.org/474216).
+ Fix assert when moving a widget to a new vertical viewspace. [Commit](http://commits.kde.org/kate/7b7270c83f1d29fb09d7cc690904fd98eee8bce9).
+ Fix crash on viewspace close. [Commit](http://commits.kde.org/kate/65b91bf45c03e5a44a137643d4e7664363ac1cd7).
{{< /details >}}
{{< details id="kcalutils" title="kcalutils" link="https://commits.kde.org/kcalutils" >}}
+ Apply the same fix for ToolTip. [Commit](http://commits.kde.org/kcalutils/6697c491c460ded37bb3c0aa8b5c5f9d6a72f2c2).
+ Convert the displayed time to local time. [Commit](http://commits.kde.org/kcalutils/63ce23d143a211fb1e67ca853ce3606037be3a9d).
+ IncidenceFormatter: fix start/end dt of recurrent event crossing DST. [Commit](http://commits.kde.org/kcalutils/fb00f08d56b65c63541af204a77322a77600f6ee). Fixes bug [#451459](https://bugs.kde.org/451459). Fixes bug [#460186](https://bugs.kde.org/460186).
+ Preserve timezone of original incidence when copy-pasting. [Commit](http://commits.kde.org/kcalutils/ddbad7221ee1c7736212887d1d64142d6d174000). Fixes bug [#448703](https://bugs.kde.org/448703).
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Sanitize certificate subject name before comparing to deviceId. [Commit](http://commits.kde.org/kdeconnect-kde/106ce36ca4aaa6909c68edc20974a51812c3cab4).
+ Fix remote keyboard sending "space" when pressing space. [Commit](http://commits.kde.org/kdeconnect-kde/98dd492f75a208f79d8ef0e6bd0b23ffaf84fd9c). Fixes bug [#473956](https://bugs.kde.org/473956).
+ Fix SFTP plugin on Windows. [Commit](http://commits.kde.org/kdeconnect-kde/393624af9c6cf1af8bf2b912a4acaeb06abdbb40). Fixes bug [#473511](https://bugs.kde.org/473511).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix audio channel setting breaking opening of existing project file. [Commit](http://commits.kde.org/kdenlive/5cb9fbbdef71563da856614537cfc10fd333e9b9).
+ Fix possible crash in audiolevel widget. [Commit](http://commits.kde.org/kdenlive/88ce7e227417d252d8988934f5316d6b1d2303e5).
+ Fix default audio channels for project not correctly saved. [Commit](http://commits.kde.org/kdenlive/deb922fba936b30aa97dccf934b3d5c507484dee).
+ Fix guide/marker categories all black on some non english locales. [Commit](http://commits.kde.org/kdenlive/59955a98d1a91c2963a7d276b101c31ad9c2922e).
+ Ensure Media browser saves zoom level when using mouse wheel to zoom. [Commit](http://commits.kde.org/kdenlive/51c925296706c27dde49edb2ae8ed7617db695cc).
+ Extract audio: export only active streams, merge all if requested. [Commit](http://commits.kde.org/kdenlive/1c3a4130d9eefe63708fac2506c07852920d9a38).
+ Fix crash on subclip transcoding. [Commit](http://commits.kde.org/kdenlive/5d60cd1f0ec2cfd00d538465a1011ef9596f473e).
+ Fix audio extract for multi stream clips. [Commit](http://commits.kde.org/kdenlive/4dae6cb0aa2edd96bf0e3f216eabde03ce9cdc59).
+ When restoring audio or video component in timeline, first try target track, then mirror track. [Commit](http://commits.kde.org/kdenlive/9bb0a1fb8aafb07743bc4b62278bc9c9fd52dfd7).
+ Fix multi guide export enabled by default. [Commit](http://commits.kde.org/kdenlive/7a65e5949dc4b562a199a945aa3e4607cb54a5be).
+ Fix guides categories all black when opening a project from a different locale. [Commit](http://commits.kde.org/kdenlive/d54d2bb0106eb8413f969161fef536d8c563b4f8).
+ Fix archiving crash on Windows caused by filesystem case sensitivity. [Commit](http://commits.kde.org/kdenlive/d331eb82e1c31e077a0050dd130defad950a4e44).
+ Project Bin: don't draw icon frame if icon size is null. [Commit](http://commits.kde.org/kdenlive/01be71dd7e3ffdfb43fed5f6aff3afee08c216ab).
+ Fix zone rendering not remembered when reopening a project. [Commit](http://commits.kde.org/kdenlive/14217917b0121bea0bcaa24d5e328976eebdebd6).
+ Fix detection/fixing when several clips in the project use the same file. [Commit](http://commits.kde.org/kdenlive/fc291091e96551a9235f987f05adf48c2ef0d6c2).
+ Correctly update guides list when switching timeline tab. [Commit](http://commits.kde.org/kdenlive/2e98e8b26005c15c43799ba6b199a5ee0af9befd).
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Correctly size constrain Apple Wallet generic pass thumbnail images. [Commit](http://commits.kde.org/kdepim-addons/1cb52b8a1b904e01d014c81e88d23e4431826820).
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Revert "Use config plugin instead of out of process config dialog". [Commit](http://commits.kde.org/kdepim-runtime/f33496a346f49e95caaf7f664ef2f91e4b7f3ddb). Fixes bug [#473897](https://bugs.kde.org/473897).
+ Revert "Fix race condition when building". [Commit](http://commits.kde.org/kdepim-runtime/d3f20b3dab530065f9cff9d73e4ca1076af55a29).
+ Vcarddirresource: fix directory not being configured on start. [Commit](http://commits.kde.org/kdepim-runtime/e6bb9d3876d9b82860be9857889298780efd9705).
+ Icaldirresource: fix directory not being configured on start. [Commit](http://commits.kde.org/kdepim-runtime/71c37db994ccb4877c9220e200b2517c5fd5b1d4). See bug [#436693](https://bugs.kde.org/436693).
{{< /details >}}
{{< details id="kgpg" title="kgpg" link="https://commits.kde.org/kgpg" >}}
+ Require akonadi-contacts >= 23.07.80. [Commit](http://commits.kde.org/kgpg/dde1784a68c151faf85e7804e1a4c4a7a6821af0).
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Thumbnail: Fix heap-use-after-free in AudioCreator::create. [Commit](http://commits.kde.org/kio-extras/cf5d29ae48c627d6299638a5c535f5d8c2ae36fa). Fixes bug [#469458](https://bugs.kde.org/469458).
+ Fix crash with corrupted EXR images. [Commit](http://commits.kde.org/kio-extras/73cb34263e582e5ebef1bbb7bb927f6b1756350f).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Also allow direct extraction from SNCF Secutix barcodes. [Commit](http://commits.kde.org/kitinerary/ebdd1750d069220cdef3e09eba48be260c1606d5). See bug [#474197](https://bugs.kde.org/474197).
+ Correctly update the search offset in the MAV extractor. [Commit](http://commits.kde.org/kitinerary/2616b436af854fd366a8e028335d4aef686b3cb8).
+ Handle more cases of ZSSK network tickets. [Commit](http://commits.kde.org/kitinerary/add3cb208de8d15ff45c0ee0a5b0b23240175d4d).
+ Correctly import buses from DB online tickets. [Commit](http://commits.kde.org/kitinerary/3f3df8c761f777472b945053ead3f0fd2251d93f).
+ Add Slovak Lines PDF ticket extractor script. [Commit](http://commits.kde.org/kitinerary/fd25bb9bac38539cb5e4ad16c1230239e268dfb2).
+ Add premiumkino Apple Wallet pass extractor script. [Commit](http://commits.kde.org/kitinerary/d2d291e2613ad525cbe70cc11d9cd80e21a654a1).
+ Handle more end of validtity variants in RCT2 tickets. [Commit](http://commits.kde.org/kitinerary/dc27b34a6c0fa48add898924bb311367e3ad8be0).
+ Fall back to the second RCT2 title line if the first one is empty. [Commit](http://commits.kde.org/kitinerary/bd60532af48ac661d962de7fba8fd7201422c97e). Fixes bug [#472916](https://bugs.kde.org/472916).
+ Handle Amtrak tickets with unreserved seats. [Commit](http://commits.kde.org/kitinerary/0f6f9f2fca71fd6d8acdbc6dba42d31b68db7c23).
+ Add British Airways boarding pass extractor script. [Commit](http://commits.kde.org/kitinerary/bdccf15cc191de08d581faa79095a4cbe478d6f6).
+ Add extractor script for Aer Lingus PDF boarding passes. [Commit](http://commits.kde.org/kitinerary/de95bf30c33cb75d24925c49997087ca0aed5213).
+ Increase upper aspect ratio limit for PDF417 barcodes. [Commit](http://commits.kde.org/kitinerary/d0051136e88917e68e4fee6085feb24c7270c4ef).
{{< /details >}}
{{< details id="kmahjongg" title="kmahjongg" link="https://commits.kde.org/kmahjongg" >}}
+ Allow users saying they never want to get the "Do you want to save" question again. [Commit](http://commits.kde.org/kmahjongg/2f79b9cbba0e602f7a3344b63ffa4c51b913abf5).
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Fix definition of HAVE_X11. [Commit](http://commits.kde.org/kmail/8a5db4f77e36456575ff3c7cb838ceade79b9d0b).
{{< /details >}}
{{< details id="knavalbattle" title="knavalbattle" link="https://commits.kde.org/knavalbattle" >}}
+ Reset stats on restarting a game. [Commit](http://commits.kde.org/knavalbattle/9fba33485f9654078318b01bfb8007c9966f5666). Fixes bug [#473191](https://bugs.kde.org/473191).
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Fix crashes in testTerminalInterface. [Commit](http://commits.kde.org/konsole/d1393cf591a17230b263b55d2ce890e3020f13ef). Fixes bug [#473043](https://bugs.kde.org/473043).
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Handle another luggage locker tagging variant. [Commit](http://commits.kde.org/kosmindoormap/d4d96145489731616dee40b01c6ebcfedeacd36e).
+ Show the type of diplomatic entities in the info box. [Commit](http://commits.kde.org/kosmindoormap/26094b4f339e47261874bff4df776f35e930536b).
+ Add consulate icon, show labels for embassys. [Commit](http://commits.kde.org/kosmindoormap/7ce9fe9807ef4141ad9e3289f693643caec8ee3e).
+ Render food courts, clinics and an alternative travel agency tagging. [Commit](http://commits.kde.org/kosmindoormap/3c86dd972327b0c7eff19d0158802ac0ab7f0a23).
+ Handle network:wikipedia tags in the same way we handle operator:wikipedia. [Commit](http://commits.kde.org/kosmindoormap/155347bb10e1f8e68133d1ddf0865fee5e2386ab).
+ Display lost & found offices. [Commit](http://commits.kde.org/kosmindoormap/7711d9f61d58e1c3e4cbfedaff7bde981319b500).
+ Display airport baggage claim belts. [Commit](http://commits.kde.org/kosmindoormap/9243121f0d0f9d0e688219a776d093fb250d8fdb).
+ Add luggage locker icon. [Commit](http://commits.kde.org/kosmindoormap/7f48f1e0d7db5063b4b8bef7503cab63bafc233f).
{{< /details >}}
{{< details id="ktouch" title="ktouch" link="https://commits.kde.org/ktouch" >}}
+ Make string translatable. [Commit](http://commits.kde.org/ktouch/6dabf5117fe9753d94c82da2e9f03d30a11c3067).
{{< /details >}}
{{< details id="libkleo" title="libkleo" link="https://commits.kde.org/libkleo" >}}
+ Don't use auto when concatenating QStrings to avoid QStringBuilder. [Commit](http://commits.kde.org/libkleo/19b514e4611865ceca1397a46a04c10be0d879d7).
+ Bump KF5_MIN_VERSION to 5.104 for Windows. [Commit](http://commits.kde.org/libkleo/fe6c4e60034bbf0e6abe97125c071c2476f78714).
{{< /details >}}
{{< details id="libkomparediff2" title="libkomparediff2" link="https://commits.kde.org/libkomparediff2" >}}
+ Add namespace prefix to include guards. [Commit](http://commits.kde.org/libkomparediff2/5642d4ac6217f3cf5bfc99bc48b39b110b23d7cc).
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ "changed" can't be a reference since it gets modified later on the loop. [Commit](http://commits.kde.org/lokalize/399de5ec6fa066bd659622a7a231225db2cd1566). Fixes bug [#474132](https://bugs.kde.org/474132).
{{< /details >}}
{{< details id="merkuro" title="merkuro" link="https://commits.kde.org/merkuro" >}}
+ Fix ViewMenu.qml. [Commit](http://commits.kde.org/merkuro/990dc46ce6176186d954b86c29b78302d78009c0).
+ Fix segfault on launch. [Commit](http://commits.kde.org/merkuro/b15f8629599477953dba429eac57260cd67a6cc6). Fixes bug [#473618](https://bugs.kde.org/473618).
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ Use shortcode as a fallback for body when sending stickers. [Commit](http://commits.kde.org/neochat/0408956364e64fa0f1d49480c0f63cd49b70652e).
+ Add cmake check for qtlocation QML module. [Commit](http://commits.kde.org/neochat/161412cbcf43a28eade57465b936f0b4958804cc).
+ Add Windows-specific screenshots as screenshots with custom attribute. [Commit](http://commits.kde.org/neochat/4853212326b57253b3229cebdbfbaf446e8e93f3).
+ Deselect space when leaving it. [Commit](http://commits.kde.org/neochat/4886f1c3b9e6d800069a590a7391ea92f436fd30). Fixes bug [#473271](https://bugs.kde.org/473271).
+ Fix opening settings. [Commit](http://commits.kde.org/neochat/56d7d1f313a25a45ac5b19b3e8bd8540ed3037cd). Fixes bug [#473789](https://bugs.kde.org/473789).
+ Fix opening report sheet. [Commit](http://commits.kde.org/neochat/04682b077adb69af206ccc14a1d2d84cbe82db13).
+ Force plain text user display name in user detail sheet. [Commit](http://commits.kde.org/neochat/ed496e04bb51d6a42bf56f143036d799cca827f4).
+ Fix opening ban sheet. [Commit](http://commits.kde.org/neochat/8439768cc39914b9fcb72e13018546e174c5b6f1).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix creation of new JS objects. [Commit](http://commits.kde.org/okular/0f896f779d4969c7c1f04468faf197796cd712ad). Fixes bug [#474256](https://bugs.kde.org/474256).
+ Guard against setting invalid button icons. [Commit](http://commits.kde.org/okular/e7734a7849e53ea9889d443c3ebbcfb52d572fe6).
+ The nFace argument in PDF JS button icon methods is optional. [Commit](http://commits.kde.org/okular/60a634a36e5e235b14c28cce71183c76146f4d6c). See bug [#474256](https://bugs.kde.org/474256).
+ Fix cert selection height. [Commit](http://commits.kde.org/okular/4a23eae08e8dcf5ce55bbdb184a09cc8e1565c97).
{{< /details >}}
{{< details id="qmlkonsole" title="qmlkonsole" link="https://commits.kde.org/qmlkonsole" >}}
+ Pass the automatically incremented version to project. [Commit](http://commits.kde.org/qmlkonsole/8d99c3a5ba6e64eb4af4f2e5be6bf05abd557b9b).
{{< /details >}}
{{< details id="yakuake" title="yakuake" link="https://commits.kde.org/yakuake" >}}
+ Fix broken HAVE_KWAYLAND assignment. [Commit](http://commits.kde.org/yakuake/c214cfb7e1f0961afe888737c4bee604ce528e77).
{{< /details >}}
