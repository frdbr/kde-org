2007-01-21 08:36 +0000 [r625805]  reitelbach

	* branches/KDE/3.5/kdewebdev/kfilereplace/Makefile.am: fix messages
	  target. this will make 28 whatsthis help texts translateable.

2007-01-27 19:57 +0000 [r627711]  lueck

	* branches/KDE/3.5/kdewebdev/quanta/data/config/actions.rc: make it
	  translatable: replaced '&amp;' with clash(&)
	  CCMAIL:kde-i18n-doc@kde.org

2007-01-29 15:13 +0000 [r628221]  coolo

	* branches/KDE/3.5/kdewebdev/quanta/data/config/actions.rc: unbreak
	  compilation

2007-02-06 20:40 +0000 [r630937]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/utility/quantanetaccess.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Fix directory
	  creation when certain ftp servers are used. BUG: 141232

2007-02-16 13:42 +0000 [r634153]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/sagroupparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/php/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Autocompletion for
	  member variables. Patch by Andrew Lowe
	  <andrew.lowe@manildra.com.au>. Reparse the script areas before
	  $this completion (fix needed for the above feature).

2007-02-17 08:21 +0000 [r634405]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp: Unbreak
	  autocompletion for unknown XML tags (broken by the previous
	  commit). Patch by Andrew Lowe <andrew.lowe@manildra.com.au> .

2007-02-27 14:14 +0000 [r637709]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantadoc.cpp: Fix crash
	  when dropping a document template on an Untitled empty document
	  by removing the hack to destroy/recreate the document if it is
	  empty and Untitled. From my POV this case in handled, but if the
	  Kate devels want, they can reopen the report. BUG: 141908

2007-03-02 20:19 +0000 [r638661]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/parsercommon.h,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/saparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/node.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/parsercommon.cpp: Fix
	  crash when the tag editing dialog was quickly invoke after a
	  delete from the tag. Add guard for a possible crash in the
	  rebuild process. Add some code to track the node
	  creation/destruction process to help debugging parser related
	  crashes.

2007-03-02 20:58 +0000 [r638671]  lueck

	* branches/KDE/3.5/kdewebdev/quanta/Makefile.am: make the messages
	  in actions.rc translatable

2007-03-31 08:09 +0000 [r648392]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/viewmanager.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/node.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: fix crash after
	  using Close Other Tabs for a tab holding a plugin

2007-03-31 13:35 +0000 [r648474]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidget.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/node.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/node_impl.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/url.cpp:
	  Backport mailto fix and apply some other changes I had in my
	  local copy for a long time

2007-04-21 20:07 +0000 [r656598]  scripty

	* branches/KDE/3.5/kdewebdev/quanta/data/dtep/docbook-4.2/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/kde-docbook-4.1.2/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/kde-docbook-4.2/description.rc,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/cfg/klinkstatus.kcfg:
	  .rc and .kcfg files should not be executable (goutte)

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

