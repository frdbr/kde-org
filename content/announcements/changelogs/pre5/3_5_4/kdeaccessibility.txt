2006-05-27 17:19 +0000 [r545511]  lueck

	* branches/KDE/3.5/kdeaccessibility/kmag/kmag.cpp: fixed 4
	  untranslated strings CCMAIL:kde-i18n-doc@kde.org

2006-06-05 01:38 +0000 [r548257]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/plugins/epos/eposconfwidget.ui:
	  BUG:128631 Fix spelling error.

2006-06-08 09:46 +0000 [r549335]  gunnar

	* branches/KDE/3.5/kdeaccessibility/kbstateapplet/kbstate.cpp: Fix
	  the condition for the end of the list of modifier keys

2006-06-10 15:03 +0000 [r549988]  lueck

	* branches/KDE/3.5/kdeaccessibility/doc/kmag/index.docbook,
	  branches/KDE/3.5/kdeaccessibility/doc/kmousetool/index.docbook,
	  branches/KDE/3.5/kdeaccessibility/doc/kmouth/index.docbook:
	  documentation backport from trunk CCMAIL:kde-doc-english@kde.org
	  CCMAIL:kde-i18n-doc@kde.org

2006-06-10 17:26 +0000 [r550025]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerconf.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerconf.h:
	  BUG:128938 CCMAIL:kde-i18n-doc New strings. Do not store
	  translated strings in xml file.

2006-06-12 23:06 +0000 [r550856]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/chat.xml:
	  Add RegExp to remove repeating character strings, such as
	  heloooooooo

2006-06-15 00:20 +0000 [r551565]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerproc.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerconf.cpp:
	  BUG:128938 CCMAIL:kde-i18n-doc@kde.org Untranslated strings.

2006-06-15 01:25 +0000 [r551578]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/chat-de.xml,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/editreplacementwidget.ui,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/festival_unspeakable_chars.xml,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/abbreviations.xml,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/polish_festival_unspeakables.xml,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/polish_festival_fixes.xml,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerproc.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerconfwidget.ui,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerconf.cpp,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/kmail.xml,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/chat.xml,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/qt2plaintext.xml,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerproc.h,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/emoticons.xml:
	  BUG:129146 CCMAIL: kde-i18n-doc@kde.org New strings. Allow case
	  sensitivity option on matches.

2006-06-15 01:35 +0000 [r551580]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/stringreplacerconfwidget.ui:
	  Fix reversed column headings.

2006-07-01 07:39 +0000 [r556675]  binner

	* branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/kttsd.h: fix
	  duplicate parameter name

2006-07-23 16:25 +0000 [r565527]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/players/alsaplayer/Makefile.am,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/alsaplayer/alsaplayer.cpp:
	  Support for gcc-2.95 and alsa-0.9.8. Patch provided by Leo
	  Savernik.

