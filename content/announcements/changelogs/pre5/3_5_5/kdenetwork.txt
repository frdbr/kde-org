2006-07-24 14:41 +0000 [r565819]  kling

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircprotocol.cpp:
	  Plugged two memory leaks in the IRC Network Configuration dialog.

2006-07-24 21:45 +0000 [r565952]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimanager.cpp:
	  Backport fix for bug 104243: [liboscar] add contact doesn't work
	  all the time CCBUG: 104243

2006-07-25 12:34 +0000 [r566106]  kling

	* branches/KDE/3.5/kdenetwork/knewsticker/newsscroller.cpp: Fixed
	  flickering when scrolling (which is all the time.) BUG: 63494

2006-07-27 13:05 +0000 [r566891]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimanager.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimodifytask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimanager.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimodifytask.h:
	  Backport: Don't use id of items that are waiting to be sent to
	  server. Fix adding multiple items at once that was broken by
	  commit 545764. CCBUG: 104243

2006-07-27 13:28 +0000 [r566892]  kling

	* branches/KDE/3.5/kdenetwork/krfb/kinetd/kinetd.cpp: Make sure
	  PortListener::m_dnssdRegistered is initialized, or it will be
	  used uninitialized in dnssdRegister()

2006-07-27 13:45 +0000 [r566900]  kling

	* branches/KDE/3.5/kdenetwork/krfb/krfb/xupdatescanner.cc: Fixed
	  two delete/delete[] mismatches.

2006-07-27 14:16 +0000 [r566908]  kling

	* branches/KDE/3.5/kdenetwork/krfb/libvncserver/sraRegion.h,
	  branches/KDE/3.5/kdenetwork/krfb/libvncserver/sraRegion.c: Merge
	  region fixes from the LibVNCServer project, fixes a bunch of
	  nasty pointer action.

2006-07-28 09:28 +0000 [r567185]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp:
	  backport: clear away message request queue on logout

2006-07-28 13:47 +0000 [r567294]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscarcontact.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/icqcontact.cpp:
	  Backport fix for bug 124940: [liboscar] use new aliases from the
	  server. First use alias if a user doesn't have alias than use
	  nickname from user info. CCBUG: 124940

2006-07-30 20:43 +0000 [r567996]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp:
	  Backport fix for bug when ICQ or AIM status icon stays turning
	  forever if connection is closed on login BUG: 126754

2006-07-31 16:42 +0000 [r568263]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/oscartypes.h:
	  Fix for The ICQ server thinks your client is too old CCBUG:
	  130630

2006-08-01 12:12 +0000 [r568500]  kling

	* branches/KDE/3.5/kdenetwork/ksirc/dccMgrTest.cpp,
	  branches/KDE/3.5/kdenetwork/ksirc/Makefile.am: Unbreak test
	  suite. BUG: 129802

2006-08-06 12:09 +0000 [r570311]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/interface_wireless_wirelessextensions.cpp:
	  testing would be appreciated BUG:123765

2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-09 17:47 +0000 [r571507]  kling

	* branches/KDE/3.5/kdenetwork/krdc/vidmode.cpp: Check the return
	  value from XF86VidModeGetAllModeLines() to avoid crashing. BUG:
	  73519

2006-08-10 21:45 +0000 [r571854]  kling

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircprotocol.cpp:
	  Fixed a null-pointer dereference crash. BUG: 121561

2006-08-13 11:37 +0000 [r572613]  kling

	* branches/KDE/3.5/kdenetwork/krdc/krdc.cpp: Backport of SVN commit
	  572606 by bhards: Apply fix for the automatic fullscreen size
	  bug. Thanks to XuÃ¢n Baldauf for the patch. BUG: 127304

2006-08-15 22:06 +0000 [r573374]  kling

	* branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/kcminterface.ui:
	  It's UTMP, not UMTP. BUG: 88877 GUI: Though you probably don't
	  translate this one.

2006-08-17 13:36 +0000 [r573903-573902]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete (removed): Remove Kopete 0.11
	  from KDE 3.5, it will be replaced by Kopete 0.12

	* branches/KDE/3.5/kdenetwork/kopete (added),
	  branches/kopete/0.12/kopete (removed): Move the Kopete 0.12
	  branch into the KDE 3.5 tree

2006-08-17 18:25 +0000 [r573989]  ogoffart

	* branches/KDE/3.5/kdenetwork/configure.in.bot: Merge change from
	  Kopete 0.12 branch: xslt is not required anymore, and libidn fix

2006-08-17 18:30 +0000 [r573990]  ogoffart

	* branches/KDE/3.5/kdenetwork/doc/kopete/chatstyle.docbook (added),
	  branches/KDE/3.5/kdenetwork/doc/kopete/index.docbook: update
	  Kopete 0.12 doc

2006-08-17 22:44 +0000 [r574054]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/webcamtask.cpp:
	  some fixes in webcam code. That should fix bug 131218. BUG:
	  131218

2006-08-18 20:58 +0000 [r574370]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/kopete/kopetewindow.cpp: Do
	  not remove the status message if one protocol doesn't support it
	  BUG: 132609

2006-08-18 21:50 +0000 [r574390]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabberchatsession.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabbergroupchatmanager.cpp:
	  Fix Bug 132611: MUC error: Your message could not be delivered,
	  Reason: "If set, the 'from' attribute must be set to the user's
	  full JID."a Kopete was using the room nick JID instead of the
	  normal JID as the XMPP core require.

2006-08-19 17:34 +0000 [r574652]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/receivefiletask.cpp:
	  Handle simultaneous filetransfers properly. NOTE: filetransfers
	  containing several files at once are still not supported. BUG:
	  132101

2006-08-20 07:19 +0000 [r574780]  bram

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig.cpp:
	  Small spelling and style errors

2006-08-20 09:40 +0000 [r574802]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscarversionupdater.cpp:
	  Include moc file for a faster build.

2006-08-20 13:08 +0000 [r574872]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnaccount.cpp:
	  Fix a problem where you cannot remove MSN contacts. I have this
	  patch localy for months, but i forgot to commit it. Cf first part
	  of BUG 122317 (the commit r513817 was not enough) CCBUG: 122317

2006-08-20 14:11 +0000 [r574900]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/ui/dlgjabbervcard.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/ui/dlgchatroomslist.ui,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/ui/gwchatpropswidget.ui,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabberbasecontact.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscarvisibilitydialog.cpp:
	  Fix typos. thanks to Pedro for reporting them. CCMAIL:
	  morais.pedro@gmail.com

2006-08-20 18:23 +0000 [r575071]  amantia

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/Makefile.am:
	  "Fix" --enable-final (not tested).

2006-08-20 19:02 +0000 [r575088]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircprotocol.cpp:
	  Fix crash when user changes current host item to item with
	  diffrent port number in irc network configuration.

2006-08-20 19:15 +0000 [r575093]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircprotocol.cpp:
	  Don't remove port number from host item when item is moved up or
	  down.

2006-08-21 15:05 +0000 [r575435]  jriddell

	* branches/KDE/3.5/kdenetwork/COPYING-DOCS (added): Add FDL licence
	  for documentation

2006-08-26 19:47 +0000 [r577478]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/offlinemessagestask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/offlinemessagestask.h:
	  Fix bug 131718 ICQ authorization request pops up at every login.
	  Always delete offline messages from server because authorization
	  request is also treated as an offline message. Official ICQ
	  client deletes offline messages always too. BUG: 131718

2006-08-29 09:57 +0000 [r578415]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/coreprotocol.cpp:
	  Fix Groupwise on PPC

2006-08-31 11:14 +0000 [r579188]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircnetworks.xml:
	  Forward port changes overwritten by the 0.12 merge, requested by
	  hermier

2006-08-31 11:51 +0000 [r579202]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircnetworks.xml:
	  Just sync with HEAD

2006-09-03 09:23 +0000 [r580318]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/gwchatrooms.h:
	  pedantic--

2006-09-09 08:48 +0000 [r582387]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/sendpicturetask.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/client.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/yahooclientstream.h:
	  pedantic--

2006-09-09 13:51 +0000 [r582475]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/messagereceivertask.cpp:
	  Fix receiving of multiple offline messages. BUG: 133256

2006-09-11 09:41 +0000 [r582998]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp:
	  Create directories if they don't already exists. BUG: 132102

2006-09-11 10:38 +0000 [r583012]  aseigo

	* branches/KDE/3.5/kdenetwork/filesharing/simple/fileshare.cpp,
	  branches/KDE/3.5/kdenetwork/filesharing/simple/controlcenter.ui:
	  belated fix for 108624 BUG:108624

2006-09-12 12:26 +0000 [r583484]  uwolfer

	* branches/KDE/3.5/kdenetwork/kget/kget_plug_in/kget_linkview.cpp:
	  use the default kget icon (useful, if there is another icontheme
	  installed). backport from make_kget_cool branch.

2006-09-12 12:32 +0000 [r583487]  uwolfer

	* branches/KDE/3.5/kdenetwork/kget/kget_plug_in/kget_plug_in.cpp:
	  use the default kget icon (useful, if there is another icontheme
	  installed). backport from make_kget_cool branch. forgot this file
	  in last commit.

2006-09-15 12:54 +0000 [r584758]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/logintask.cpp:
	  Don't crash if the server sends us an unexpected type of field.

2006-09-15 16:53 +0000 [r584852]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/gadu/libgadu/Makefile.am:
	  the usual "unbreak compilation"

2006-09-15 20:13 +0000 [r584918]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/private/kopeteemoticons.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/private/kopeteemoticons.h:
	  Fix emoticon parsing if there are sub-emoticons. BUG: 133995
	  Patch by Martin Matusiak, thanks.

2006-09-17 10:30 +0000 [r585488]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabberbasecontact.cpp:
	  Force the display name source to be custom for Jabber contact
	  that have an alias (and for which source is not set to another
	  contact)

2006-09-17 13:32 +0000 [r585603]  kling

	* branches/KDE/3.5/kdenetwork/krdc/rdp/rdpprefs.ui,
	  branches/KDE/3.5/kdenetwork/krdc/rdp/rdphostpref.h: Adding
	  support for 8 more RDP keymaps. GUI: Adds 8 strings such as
	  "Estonian (et)"

2006-09-19 09:07 +0000 [r586263]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/configure.in.in:
	  don't write files to source directory. hopefully fixes dashboard
	  build.

2006-09-19 14:53 +0000 [r586415]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/configure.in.in:
	  forgot to remove this

2006-09-19 18:29 +0000 [r586483]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/gadu/Makefile.am:
	  find libgadu-config.h in builddir

2006-09-20 07:24 +0000 [r586638]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/filetransfernotifiertask.cpp:
	  fix uninitialized variable warning

2006-09-25 11:36 +0000 [r588225]  pfeiffer

	* branches/KDE/3.5/kdenetwork/kget/main.cpp: Show the right
	  filename for dynamic download things BUG: 134489

2006-09-29 14:09 +0000 [r590208]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/interface_wireless_wirelessextensions.cpp:
	  preparing for Kernel 2.6.19 + WE-21, as discussed in k-c-d

2006-10-02 11:08 +0000 [r591333]  coolo

	* branches/KDE/3.5/kdenetwork/kdenetwork.lsm: updates for 3.5.5

2006-10-02 12:16 +0000 [r591435]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION: version bump
