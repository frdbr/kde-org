------------------------------------------------------------------------
r1223428 | ltoscano | 2011-03-03 11:36:15 +1300 (Thu, 03 Mar 2011) | 5 lines

Backport r1223417: Use the correct mimetype for pot files.

BUG: 121798
FIXED-IN: 4.6.2

------------------------------------------------------------------------
r1224094 | kossebau | 2011-03-08 10:37:22 +1300 (Tue, 08 Mar 2011) | 1 line

changed: bumped version
------------------------------------------------------------------------
r1225382 | scripty | 2011-03-21 02:13:07 +1300 (Mon, 21 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1225624 | sars | 2011-03-22 19:06:11 +1300 (Tue, 22 Mar 2011) | 3 lines

Fix failed build with non gcc compiler also in 4.6 branch

BUG: 265169
------------------------------------------------------------------------
r1226279 | scripty | 2011-03-29 01:09:19 +1300 (Tue, 29 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1226417 | scripty | 2011-03-30 07:20:51 +1300 (Wed, 30 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
