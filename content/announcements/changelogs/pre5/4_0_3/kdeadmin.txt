2008-03-01 17:51 +0000 [r780947]  lueck

	* branches/KDE/4.0/kdeadmin/doc/kpackage/index.docbook: backport
	  from trunk

2008-03-02 21:20 +0000 [r781439]  reitelbach

	* branches/KDE/4.0/kdeadmin/kdat/Tape.cpp: Don't use word-puzzles.

2008-03-25 17:30 +0000 [r789923]  reitelbach

	* branches/KDE/4.0/kdeadmin/kdat/Tape.cpp: Fix Typo in % argument.
	  i18n-list: I didn't ask for permission to change this during
	  string freeze because KDat is not even built by default
	  (commented out in the CMakefile). CCMAIL:kde-i18n-doc@kde.org

