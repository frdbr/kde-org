<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdeaccessibility-4.0.0.tar.bz2">kdeaccessibility-4.0.0</a></td><td align="right">6.1MB</td><td><tt>aaf6e7d2e79ca7de73a0f3b13e498c32</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdeadmin-4.0.0.tar.bz2">kdeadmin-4.0.0</a></td><td align="right">1.3MB</td><td><tt>4ee63cb1ab7550868ccd8b2a1cfcb1f4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdeartwork-4.0.0.tar.bz2">kdeartwork-4.0.0</a></td><td align="right">41MB</td><td><tt>140a1ba06c57b8d4ca2a2331e60840c5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdebase-4.0.0.tar.bz2">kdebase-4.0.0</a></td><td align="right">3.9MB</td><td><tt>01d8f2f16cbd4e225efc996b0dd39769</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdebase-runtime-4.0.0.tar.bz2">kdebase-runtime-4.0.0</a></td><td align="right">46MB</td><td><tt>da93f59497ff90ad01bd4ab9b458f6cb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdebase-workspace-4.0.0.tar.bz2">kdebase-workspace-4.0.0</a></td><td align="right">29MB</td><td><tt>f3d2155ff5ff7472a8884bd3f31bff16</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdebindings-4.0.0.tar.bz2">kdebindings-4.0.0</a></td><td align="right">3.8MB</td><td><tt>4cc820a19097c8f180277aa9ae0c9385</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdeedu-4.0.0.tar.bz2">kdeedu-4.0.0</a></td><td align="right">40MB</td><td><tt>73924e158e4a2de2107be441c808251f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdegames-4.0.0.tar.bz2">kdegames-4.0.0</a></td><td align="right">23MB</td><td><tt>6264c0034f6389a2807a4e1723ba1c81</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdegraphics-4.0.0.tar.bz2">kdegraphics-4.0.0</a></td><td align="right">2.3MB</td><td><tt>6cad7b165d99c43d1a19a0350598821c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdelibs-4.0.0.tar.bz2">kdelibs-4.0.0</a></td><td align="right">8.7MB</td><td><tt>79d0f83ca81fc4a135663943340c0b8f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdemultimedia-4.0.0.tar.bz2">kdemultimedia-4.0.0</a></td><td align="right">1.1MB</td><td><tt>0bf1cd18a23017a37324d9f8c4902e19</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdenetwork-4.0.0.tar.bz2">kdenetwork-4.0.0</a></td><td align="right">6.1MB</td><td><tt>f362bd34b589800845abfb99589d4cfc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdepimlibs-4.0.0.tar.bz2">kdepimlibs-4.0.0</a></td><td align="right">1.7MB</td><td><tt>1a68662230fcd4ec8cea90bb780f920e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdesdk-4.0.0.tar.bz2">kdesdk-4.0.0</a></td><td align="right">4.1MB</td><td><tt>7777425fe349a25c78b808e3fc04165a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdetoys-4.0.0.tar.bz2">kdetoys-4.0.0</a></td><td align="right">2.2MB</td><td><tt>6e4e2eea3d87718f48716f975b48ada2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdeutils-4.0.0.tar.bz2">kdeutils-4.0.0</a></td><td align="right">2.3MB</td><td><tt>5815625f215ff3be47a21074d2c047a0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.0.0/src/kdewebdev-4.0.0.tar.bz2">kdewebdev-4.0.0</a></td><td align="right">1.2MB</td><td><tt>1c973739bdcb50befee96ddb33ad30de</tt></td></tr>
</table>
