---
version: "5.4.95"
title: "Plasma 5.4.95 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.5_Errata
    name: 5.5 Errata
type: info/plasma5
unstable: true
---


This is a beta release of Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the [Plasma 5.4.95 announcement](/announcements/plasma-5.4.95).
