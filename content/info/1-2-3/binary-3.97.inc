<ul>

<li>
 <a href="http://www.debian.org/">Debian</a>
    <ul type="disc">
      <li>
         In Debian Experimental
      </li>
    </ul>
  <p />
</li>
<li>
 <a href="http://kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         <a href="http://kubuntu.org/announcements/kde4-rc2.php">Packages for 7.10 (Gutsy)</a> including Kubuntu KDE 4 RC 2 Live CD
      </li>
    </ul>
  <p />
</li>
<li>
 <a href="http://www.mandriva.com/">Mandriva</a>
    <ul type="disc">
      <li>
         2008: <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.97/Mandriva/">Intel i386</a>
      </li>
    </ul>
  <p />
</li>

<li>
 <a href="http://www.opensuse.org/">openSUSE</a>
    <ul type="disc">
      <li>
         <a href="http://en.opensuse.org/KDE/KDE4">Packages</a>
      </li>
      <li>
         <a href="http://home.kde.org/~binner/kde-four-live/">openSUSE based KDE Four Live CD</a>
      </li>
    </ul>
  <p />
</li>

<li>
 <a href="http://www.fedoraproject.org/">Fedora</a>
    <ul type="disc">
      <li>In Rawhide</li>
    </ul>
  <p />
</li>
</ul>
