KDE Security Advisory: KGet Directory Traversal and Insecure File Operation
Vulnerabilities
Original Release Date: 2010-05-13
URL: http://www.kde.org/info/security/advisory-20100513-1.txt

0. References:
	CVE-2010-1000
    CVE-2010-1511
    SA39528

1. Systems affected:

	KGet as shipped with KDE SC 4.0.0 up to including KDE SC 4.4.3. Earlier
    versions of KDE SC may also be affected.

2. Overview:

    1) The "name" attribute of the "file" element of metalink files is not
    properly sanitized before being used to download files. If a user is
    tricked into downloading from a specially-crafted metalink file, this can
    be exploited to download files to directories outside of the intended
    download directory via directory traversal attacks. (CVE-2010-1000)

    2) In some versions of KGet (2.4.2) a dialog box is displayed allowing the
    user to choose the file to download out of the options offered by the
    metalink file. However, KGet will simply go ahead and start the download
    after some time - even without prior acknowledgment of the user, and
    overwriting already-existing files of the same name. (CVE-2010-1511)

    The vulnerabilities were reported by and the above text provided by Stefan
    Cornelius of Secunia Research. 

3. Impact:

    1) Files may be created or overwritten in directories outside of a user's
    intended download directory.

    2) Files may be created or overwritten in a user's intended download
    directory without acknowledgement of the user.

4. Solution:

	Source code patches have been made available which fix these
    vulnerabilities. At the time of this writing most OS vendor / binary
    package providers should have updated binary packages. Contact your OS
    vendor / binary package provider for information about how to obtain
    updated binary packages.

5. Patch:

    Patches have been committed to the KDE Subversion repository in the
    following revision numbers:

    4.3 branch: r1126227
    4.4 branch: r1124974
    Trunk: r1124976

    Patches for KDE SC 4.3 and KDE SC 4.4 may be obtained directory from the
    Subversion repository (no checkout needed) with the following command and
    reference SHA1 sums:

    4.3 branch: dc1b2af664fb4c74c018e9c6b02859b5c42ecd65
    svn diff -r 1126226:1126227 \
    svn://anonsvn.kde.org/home/kde/branches/KDE/4.3/kdenetwork

    4.4 branch: 3ed1b2333ba324e1fc6c1994cef1715eb0b6f457
    svn diff -r 1124973:1124974 \
    svn://anonsvn.kde.org/home/kde/branches/KDE/4.4/kdenetwork

