---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: O KDE Lança as Aplicações do KDE 17.08.1
layout: application
title: O KDE Lança as Aplicações do KDE 17.08.1
version: 17.08.1
---
7 de Setembro de 2017. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../17.08.0'>Aplicações do KDE 17.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem as melhorias no Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello, nos jogos do KDE, entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.36.
