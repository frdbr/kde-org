---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE stelt KDE Applicaties 16.08.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.08.2 beschikbaar
version: 16.08.2
---
13 oktober 2016. Vandaag heeft KDE de tweede update voor stabiliteit vrijgegeven voor <a href='../16.08.0'>KDE Applicaties 16.08</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 30 aangegeven reparaties van bugs, inclusief verbeteringen aan kdepim, ark, dolphin, kgpg, kolourpaint, okular, naast andere.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.25.
