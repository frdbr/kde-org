---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE stelt Applicaties 14.12.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 14.12.2 beschikbaar
version: 14.12.2
---
3 februari 2015. Vandaag heeft KDE de tweede stabiele update vrijgegeven voor <a href='../14.12.0'>KDE Applicaties 14.12</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Er zijn meer dan 20 aangegeven bugreparaties inclusief verbeteringen aan het anagramspel Kanagram, het UML-modelleerprogramma Umbrello, de documentviewer Okular en virtuele globe Marble aangebracht.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van Plasma Workspaces 4.11.16, KDE Development Platform 4.14.5 en de Kontact Suite 4.14.5.
