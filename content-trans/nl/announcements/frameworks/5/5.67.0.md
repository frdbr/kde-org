---
aliases:
- ../../kde-frameworks-5.67.0
date: 2020-02-02
layout: framework
libCount: 70
---
### Algemeen

- Overzetten van vele in Qt 5.15 afgekeurde methoden, dit vermindert het aantal waarschuwingen tijdens het bouwen.

### Baloo

- Configuratie migreren van KConfig naar KConfigXt om KCM in staat te stellen het te gebruiken

### Breeze pictogrammen

- Breeze stijl Kate-pictogram gebaseerd op nieuw ontwerp door Tyson Tan
- VLC-pictogram wijzigen om meer te lijken op officiële VLC-pictogrammen
- ktrip-pictogram toevoegen uit ktrip-opslagruimte
- App-pictogrammen voor toepassing/sql
- Opschonen en toevoegen van 22px media herhaal pictogrammen
- Pictogram voor text/vnd.kde.kcrash-report toevoegen
- Van application/x-ms-shortcut een actueel sneltoetspictogram maken

### Extra CMake-modules

- Ontbrekende import Env-variabele toevoegen
- ECMAddAppIcon: sc toevoegen in regex om extensie uit geldige namen te halen
- ECMAddQch: K_DOXYGEN macrogebruik ondersteunen &amp; documenteren

### Frameworkintegratie

- Ongebruikte afhankelijkheid QtDBus laten vallen

### KActivitiesStats

- Gebroken SQL query in allResourcesQuery repareren

### KActivities

- Bestanden verwijderen die Windows niet kan behandelen
- Nagaan om hulpbron-uri op te slaan zonder een afsluitende slash

### KDE Doxygen hulpmiddelen

- Module importeren verbreken voor Python2
- Utf-8 hard coderen als codering van bestandssysteem met Python2 om api.kde.org te helpen

### KCMUtils

- de nieuwe kcm-plug-ins prefereren boven de oude
- KCModuleQml: Maak dat standaard zeker wordt uitgestuurd met de huidige configModule-&gt;representsDefaults bij laden
- Knop tonen die wat is gedeclareerd door KCModule respecteert
- KPluginSelector bijwerken om KCM in staat te stellen de goede status voor resetten, toepassen en standaard knop te tonen

### KConfig

- KConfigXT code opschonen
- Python bindings bouwen repareren na ebd14f29f8052ff5119bf97b42e61f404f223615
- KCONFIG_ADD_KCFG_FILES: ook op nieuwe versie van kconfig_compiler opnieuw genereren
- Toestaan om ook een target in plaats van een lijst met bronnen door te geven aan KCONFIG_ADD_KCFG_FILES
- KSharedConfig::openStateConfig toevoegen voor opslaan van statusinformatie
- Python bindings compilatie na 7ab8275bdb56882692846d046a5bbeca5795b009 repareren

### KConfigWidgets

- KStandardAction: methode toevoegen voor actie aanmaken van SwitchApplicationLanguage
- [KColorSchemeManager] duplicaten niet tonen
- [KColorschemeManager] optie toevoegen om het volgende globale thema toe te voegen

### KCoreAddons

- laadfouten van plug-ins verlagen van waarschuwing naar debugniveau + herverwoording
- Hoe te filteren door servicetype op de juiste manier documenteren
- perlSplit() overladen die een QRegularExpression neemt toevoegen en diegene QRegExp afkeuren
- MIME-type toevoegen voor backtraces opgeslagen door DrKonqi
- Hulpmiddeltekstfunctie KShell::tildeCollapse toevoegen
- KPluginMetaData: initialPreference() getter toevoegen
- desktoptojson: ook InitialPreference-sleutel converteren

### KDeclarative

- Onderste marge juist berekenen voor rastergedelegeerden met ondertitels
- [ConfigModule] welk pakket ongeldig is vertellen

### KHolidays

- Vakantiedagen bijwerken en vlagdagen en naamdagen voor Zweden toevoegen

### KI18n

- ki18n_wrap_ui: fout wanneer bestand niet bestaat
- [Kuit] wijzigingen in parseUiMarker() terugdraaien

### KIO

- Ontbrekende hernoemde gebeurtenis toevoegen wanneer een bestemmingsbestand al bestaat
- KFilePlacesModel: bij nieuw profiel in recente alleen recente gebruikte:/ gebaseerde items standaard tonen
- KFileCustomDialog constructor met een startDir-parameter toevoegen
- Gebruik van QRegularExpression::wildcardToRegularExpression() repareren
- Toepassingen toestaan te werken met Terminal=True in hun desktop-bestand, behandel hun bijbehorend MIME-type op de juiste manier (bug 410506)
- KOpenWithDialog: een nieuw gemaakte KService toestaan om een bijbehorend MIME-type terug te geven
- KIO::DropJobFlag toevoegen om handmatig toe te staan het menu te tonen (bug 415917)
- [KOpenWithDialog] in te vouwen groepsvak verbergen wanneer alle opties erin verborgen zijn (bug 415510)
- Effectieve verwijdering van KUrlPixmapProvider uit API terugdraaien
- SlaveBase::dispatchLoop: berekening van timeout repareren (bug 392768)
- [KDirOperator] hernoemen van bestanden toestaan vanuit het contextmenu (bug 189482)
- Dialoog voor bestand hernoemen uit upstream Dolphin (bug 189482)
- KFilePlaceEditDialog: logica in isIconEditable() verplaatsen

### Kirigami

- Het om te klappen ouderitem afkappen (bug 416877)
- Bovenste marge van kop uit private ScrollView verwijderen
- hints voor juiste grootte van de rasterindeling (bug 416860)
- aangeplakte eigenschap gebruiken voor isCurrentPage
- Een aantal waarschuwingen verwijderen
- de cursor binnen het venster proberen te houden bij typen in een OverlaySheet
- fillWidth items in mobiele modus op de juiste manier uitvouwen
- Achtergrondkleuren voor actief, koppeling, bezocht, negatief, neutraal en positief toevoegen
- Overloopknoppictogramnaam van ActionToolBar tonen
- QQC2-pagina gebruiken als basis voor Kirigami pagina
- Van waar de code vandaan komt specificeren als URL
- AbstractApplicationHeader niet blind verankeren
- pooled uitsturen nadat de eigenschappen opnieuw zijn toegekend
- opnieuw gebruikte en gepoolde signalen zoals TableView toevoegen

### KJS

- Uitvoering met machine afbreken nadat een timeoutsignaal is gezien
- Teken voor machtsverheffen ** ondersteunen uit ECMAScript 2016
- Functie shouldExcept() die werkt gebaseerd op een functie toegevoegd

### KNewStuff

- Van de functionaliteit KNSQuick::Engine::changedEntries zorgen dat deze niet breekt

### KNotification

- Nieuw signaal voor standaard activatie van actie toevoegen
- Afhankelijkheid van KF5Codecs laten vallen door de nieuwe functie stripRichText te gebruiken
- Opgemaakte tekst op Windows verwijderen
- Aan Qt 5.14 Android wijzigingen aanpassen
- raiseWidget afkeuren
- KNotification overbrengen uit KWindowSystem

### KPeople

- metainfo.yaml aan nieuwe "tier" aanpassen
- Verouderde code voor laden plug-in verwijderen

### KQuickCharts

- Qt-version controle repareren
- QAbstractItemModel registreren als anoniem type voor toekenningen van eigenschap
- De lijn van een lijngrafiek verbergen als zijn breedte is ingesteld op 0

### Kross

- addHelpOption voegt al toe door kaboutdata

### KService

- Meerdere waarden in XDG_CURRENT_DESKTOP ondersteunen
- allowAsDefault afkeuren
- De "Standaard toepassingen" in mimeapps.list de toepassingen met voorkeur maken (bug 403499)

### KTextEditor

- "woordaanvulling verbeteren om accentuering te gebruiken om woordgrenzen te detecteren" terugdraaien (bug 412502)
- finale breeze pictogrammen importeren
- Melding-gerelateerde methoden: meer member-function-pointer-based verbinding gebruiken
- DocumentPrivate::postMessage: meerdere "hash lookups" vermijden
- Functie Slepen&amp;kopiëren repareren (door Ctrl-toets te gebruiken) (bug 413848)
- ga na dat we een vierkante pictogram hebben
- juiste pictogram voor Kate instellen in over-dialoog voor KatePart
- inline notes: underMouse() juist instellen voor inline notities
- gebruik van oude mascot ATM vermijden
- Uitvouwen van variabele: variabele PercentEncoded toevoegen (bug 416509)
- Crash in variabele uitbreiden repareren (gebruikt door externe hulpmiddelen)
- KateMessageWidget: ongebruikte installatie van gebeurtenisfilter verwijderen

### KTextWidgets

- KWindowSystem afhankelijkheid laten vallen

### KWallet Framework

- readEntryList() terugdraaien om QRegExp::Wildcard te gebruiken
- Gebruik van QRegularExpression::wildcardToRegularExpression() repareren

### KWidgetsAddons

- [KMessageWidget] de juiste marge aftrekken
- [KMessageBox] tekst selecteren alleen toestaan in het dialoogvak met de muis (bug 416204)
- [KMessageWidget] devicePixelRatioF gebruiken voor animatiepixmap (bug 415528)

### KWindowSystem

- [KWindowShadows] op X-verbinding controleren
- Schaduw API introduceren
- KWindowEffects::markAsDashboard() afkeuren

### KXMLGUI

- KStandardAction gemaksmethode voor switchApplicationLanguage gebruiken
- Eigenschap programLogo toestaan om ook een QIcon te zijn
- Mogelijkheid om bugs te rapporteren tegen willekeurige zaken uit een statische lijst verwijderen
- Compilerinformatie verwijderen uit bugrapportdialoog
- KMainWindow: autoSaveSettings repareren om QDockWidgets te vangen die opnieuw worden getoond
- i18n: meer semantische contexttekenreeksen toevoegen
- i18n: vertalingen afsplitsen voor tekenreeksen "Translation"

### Plasma Framework

- Tekstballonhoekjes gerepareerd en onnuttige kleurattributen verwijderd
- hard gecodeerde kleuren in SVG's voor achtergrond verwijderd
- De grootte en pixeluitlijning van keuzevakjes en keuzerondjes repareren
- Schaduw van breeze-thema bijwerken
- [Plasma Quick] WaylandIntegratieklasse toevoegen
- Zelfde gedrag voor schuifbalk als de bureaubladstijl
- Van KPluginMetaData gebruikmaken waar we kunnen
- Menu-item voor bewerkingsmodus toevoegen aan contextmenu van bureaubladwidget
- Consistentie: gekleurde geselecteerde knoppen
- Zet endl over naar \n Niet nodig om te schonen omdat QTextStream QFile gebruikt die schoont wanneer het wordt verwijderd

### Omschrijving

- Gebruik van QRegularExpression::wildcardToRegularExpression() repareren

### QQC2StyleBridge

- Aan schuifbalk gerelateerde workarounds uit lijstgedelegeerden verwijderen
- [TabBar] frame verwijderen
- Achtergrondkleuren voor actief, koppeling, bezocht, negatief, neutraal en positief toevoegen
- hasTransientTouchInput gebruiken
- x en y altijd afronden
- mobiel-modusschuifbalk ondersteunen
- ScrollView: schuifbalken niet over inhoud leggen

### Solid

- Signalen voor udev-gebeurtenissen met acties binden en ontbinden toevoegen
- Referentie maken van DeviceInterface verhelderen (bug 414200)

### Accentuering van syntaxis

- nasm.xml wordt bijgewerkt met de laatste instructies
- Perl: 'say' aan sleutelwoordlijst toevoegen
- cmake: reguliere expressie <code>CMAKE*POLICY**_CMP&amp;lt;N&amp;gt;</code> repareren en speciale argumenten toevoegen aan <code>get_cmake_property</code>
- GraphQL definitie van accentuering toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
