---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDE publie la version 19.08 des applications.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE publie la version 19.08.3 des applications.
version: 19.08.3
---
{{% i18n_date %}}

Aujourd'hui, KDE a publié la troisième mise à jour de consolidation pour les <a href='../19.08.0'>applications de KDE %[2 ]s</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traductions, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus d'une douzaine de corrections de bogues identifiés, portant sur des améliorations de Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle, Umbrello parmi bien d'autres.

Les améliorations incluses sont :

- Dans l'éditeur de vidéos, Kdenlive, les compositions ne disparaissent plus lors de la réouverture d'un projet avec des pistes verrouillées.
- L'affichage des annotations d'Okular affiche maintenant les heures de création dans le fuseau horaire local plutôt qu'en heure « UTC ».
- Le contrôle de clavier a été amélioré dans l'utilitaire de copie d'écran, Spectacle.
