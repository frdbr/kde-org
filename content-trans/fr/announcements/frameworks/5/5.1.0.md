---
aliases:
- ../../kde-frameworks-5.1
- ./5.1
customIntro: true
date: '2014-08-08'
description: KDE publie la seconde version des environnements de développement version
  5.
layout: framework
qtversion: 5.2
title: Deuxième publication de KDE Frameworks 5
---
07 Août 2014. KDE annonce aujourd'hui la publication de la seconde version de l'environnement de développement de KDE 5. En cohérence avec la politique de planification des versions de KDE, cette version est publiée un mois après la version initiale et propose à la fois des corrections de bogues et de nouvelles fonctionnalités. 

{{% i18n "annc-frameworks-intro" "60" "/announcements/frameworks/5/5.0" %}}

## {{< i18n "annc-frameworks-new" >}}

This release, versioned 5.1, comes with a number of bugfixes and new features including:

- KTextEditor: Major refactorings and improvements of the vi-mode
- KAuth: Now based on PolkitQt5-1
- New migration agent for KWallet
- Windows compilation fixes
- Translation fixes
- New install dir for KXmlGui files and for AppStream metainfo
- <a href='http://www.proli.net/2014/08/04/taking-advantage-of-opengl-from-plasma/'>Plasma Taking advantage of OpenGL</a>
