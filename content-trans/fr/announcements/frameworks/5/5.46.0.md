---
aliases:
- ../../kde-frameworks-5.46.0
date: 2018-05-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Avoid infinite loops when fetching the URL from DocumentUrlDB (bug 393181)
- Add Baloo DBus signals for moved or removed files
- Install pri file for qmake support &amp; document it in metainfo.yaml
- baloodb : ajout d'une commande de nettoyage
- balooshow: Colorize only when attached to terminal
- Suppression de « FSUtils::getDirectoryFileSystem »
- Avoid hardcoding of filesystems supporting CoW
- Permettre la désactivation de « CoW » sur échec lorsque non pris en charge par le système
- databasesanitizer: Use flags for filtering
- Fix merging of terms in the AdvancedQueryParser
- Use QStorageInfo instead of a homegrown implementation
- Nettoyeur : amélioration de l'élaboration de la liste des périphériques
- Immediately apply termInConstruction when term is complete
- Handle adjacent special characters correctly (bug 392620)
- Add test case for parsing of double opening '((' (bug 392620)
- Utiliser « statbuf » de façon systématique

### Icônes « Breeze »

- Ajout d'une icône de boîte à miniatures pour le système d'intégration du navigateur avec Plasma
- Ajout d'une icône « Virt-manager », avec les remerciements à « ndavis »
- Ajout de « video-card-inactive »
- overflow-menu as view-more-symbolic, and horizontal
- Utiliser l'icône la plus appropriée « Deux curseurs » pour « configure »

### Modules additionnels « CMake »

- Include FeatureSummary before calling set_package_properties
- Don't install plugins within lib on android
- Rendre possible la construction de plusieurs paquets « apk » hors d'un projet
- Vérification que le paquet de l'application « androiddeployqt » possède bien un symbole « main() »

### KCompletion

- [KLineEdit] Use Qt's built-in clear button functionality
- Correction de « KCompletionBox » sur « Wayland »

### KCoreAddons

- [KUser] Check whether .face.icon is actually readable before returning it
- Make KJob signals public so Qt5 connect syntax can work

### KDeclarative

- Chargement d'une réinitialisation du pilote graphique « NV » selon la configuration
- [KUserProxy] Adjust to accounts service (bug 384107)
- Optimisations de Plasma sur plate-forme mobile
- Faire de la place pour le pied de page et l'en-tête
- Nouvelle règle pour le re-dimensionnement (bogue 391910)
- Prise en charge de la visibilité des actions
- Support nvidia reset notifications in QtQuickViews

### KDED

- Add platform detection and adjustment to kded (automatic setting of $QT_QPA_PLATFORM)

### KFileMetaData

- Add description and purpose to Xattr dep
- extractors: Hide warnings from system headers
- fix detection of taglib when compiling for Android
- Install pri file for qmake support &amp; document it in metainfo.yaml
- Permettre le retour à la ligne automatique pour les chaînes concaténées
- ffmpegextractor : rendre silencieux les alarmes d'obsolescence 
- taglibextractor : correction du bogue de style
- Gestion de plus d'étiquettes dans « taglibextractor »

### KHolidays

- holidays/plan2/holiday_sk_sk - Teacher's Day fix (bug 393245)

### KI18n

- [API dox] New UI marker @info:placeholder
- [API dox] New UI marker @item:valuesuffix
- Don't need to run previous iterations commands again (bug 393141)

### KImageFormats

- [XCF/GIMP loader] Raise maximimum allowed image size to 32767x32767 on 64 bit platforms (bug 391970)

### KIO

- Thumbnail smooth scaling in filepicker (bug 345578)
- KFileWidget: Perfectly align filename widget with icon view
- KFileWidget: Save places panel width also after hiding panel
- KFileWidget: Prevent places panel width from growing 1px iteratively
- KFileWidget: Disable zoom buttons once reached minimum or maximum
- KFileWidget: Set minimum size for zoom slider
- Ne pas sélectionner d'extension de fichier
- concatPaths: process empty path1 correctly
- Improve grid icon layout in filepicker dialog (bug 334099)
- Hide KUrlNavigatorProtocolCombo if there is just one protocol supported
- Only show supported schemes in KUrlNavigatorProtocolCombo
- Filepicker reads thumbs preview from Dolphin settings (bug 318493)
- Ajouter « Bureau » et « Téléchargements » dans la liste par défaut des emplacements
- KRecentDocument now stores QGuiApplication::desktopFileName instead of applicationName
- [KUrlNavigatorButton] Also don't stat MTP
- getxattr takes 6 parameters in macOS (bug 393304)
- Ajout d'un élément de menu « Recharger » au menu contextuel « KDirOperator » (bogue 199994)
- Save the dialog view settings even when canceling (bug 209559)
- [KFileWidget] Hardcode example user name
- Don't show top "Open With" app for folders; only for files
- Detect incorrect parameter in findProtocol
- Utiliser le texte « Autres applications... » dans le sous-menu « Ouvrir avec »
- Correctly encode URL of thumbnails (bug 393015)
- Tweak column widths in tree view of file open/save dialogs (bug 96638)

### Kirigami

- Don't warn when using Page {} outside of a pageStack
- Rework InlineMessages to address a number of issues
- Correction pour Qt 5.11
- Se reposer sur des unités pour la taille des boutons d'outils
- Colorer l'icône de fermeture sur le survol
- show a margin under the footer when needed
- Correction de « isMobile »
- Ajout d'une atténuation pour les animations d'ouverture et de fermeture
- include the dbus stuff only on unix-non android, non apple
- Surveillance de « tabletMode » à partir de KWin
- on desktop mode show actions on hover (bug 364383)
- Poignée dans le haut de la barre d'outils
- Utiliser un bouton gris de fermeture
- Réduction de la dépendance à « applicationwindow »
- Moins d'alarmes sans « applicationwindow »
- Fonctionne correctement sans « applicationWindow »
- Don't have a non-integral size on separators
- Ne pas afficher les actions lorsqu'elles sont désactivées
- Éléments vérifiables dans « FormLayout »
- Utilisation d'icônes différentes dans un exemple d'ensemble de couleurs
- Inclure seulement les icônes pour Android
- Mise en fonction avec Qt 5.7

### KNewStuff

- Fix double margins around DownloadDialog
- Fix hints in UI files about subclasses of custom widgets
- Ne pas proposer le module externe « qml » comme une cible de lien

### Environnement de développement « KPackage »

- Utilisation de « KDE_INSTALL_DATADIR » à la place de « FULL_DATADIR »
- Ajout des « URL » de donation pour tester les données

### KPeople

- Fix PersonSortFilterProxyModel filtering

### Kross

- Rendre aussi l'installation optionnelle des documentations traduites

### KRunner

- DBus runner servicename wildcard support

### KTextEditor

- optimization of KTextEditor::DocumentPrivate::views()
- [ktexteditor] much faster positionFromCursor
- Implement single click on line number to select line of text
- Fix missing bold/italic/... markup with modern Qt versions (&gt;= 5.9)

### Environnement de développement de Plasma

- Fix not shown event marker in calendar with air &amp; oxygen themes
- Use "Configure %1..." for text of applet configure action
- [Button Styles] Fill height and vertical align (bug 393388)
- add video-card-inactive icon for system tray
- Correction l'apparence pour les boutons à plat
- [Containment Interface] Don't enter edit mode when immutable
- make sure largespacing is perfect multiple of small
- call addContainment with proper paramenters
- Ne pas afficher l'arrière-plan si « Button.flat »
- ensure the containment we created has the activity we asked for
- add a version containmentForScreen with activity
- Don't alter memory management to hide an item (bug 391642)

### Motif

- S'assurer qu'un certain espacement vertical est fourni aux modules externes de configuration
- Portage de la configuration du module externe « KDEConnect » vers « QQC2 »
- Portage de « AlternativesView » vers « QQC2 »

### QQC2StyleBridge

- export layout paddings from qstyle, start from Control
- [ComboBox] Correction de la gestion de la molette de la souris
- make the mousearea not interfere with controls
- Correction de « acceptableInput »

### Opaque

- Update mount point after mount operations (bug 370975)
- Invalidate property cache when an interface is removed
- Avoid creating duplicate property entries in the cache
- [UDisks] Optimize several property checks
- [UDisks] Correct handling of removable file systems (bug 389479)

### Sonnet

- Correction de la suppression du bouton « Activé » « Désactivé »
- Correction du bouton pour addition de « Activé » « Désactivé »
- Rechercher dans les sous-dossiers pour les dictionnaires

### Coloration syntaxique

- Mettre à jour l'«  URL » du projet
- « Titre » est un commentaire, ainsi, le référencer avec « dsComment »
- Ajout d'une coloration syntaxique pour les listings de commandes « GDB » et les fichiers « gdbinit »
- Ajout d'une coloration syntaxique pour « Logcat »

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
