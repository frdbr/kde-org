---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE wydało Aplikacje KDE 16.12.2
layout: application
title: KDE wydało Aplikacje KDE 16.12.2
version: 16.12.2
---
9 lutego 2017. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../16.12.0'>Aplikacji KDE 16.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 20 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdepim, dolphin, kate, kdenlive, ktouch, okular oraz innych.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.29.
