---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE wydało Aplikacje 18.12.1.
layout: application
major_version: '18.12'
title: KDE wydało Aplikacje KDE 18.12.1
version: 18.12.1
---
{{% i18n_date %}}

Dzisiaj KDE wydało pierwszą aktualizację stabilności <a href='../18.12.0'> dla  Aplikacji KDE 18.12</a> . To wydanie zawiera tylko poprawki błędów i  aktualizacje tłumaczeń, zapewniając bezpieczną i przyjemną aktualizację  dla wszystkich.

About 20 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole, Okular, among others.

Wśród ulepszeń znajdują się:

- Akregator now works with WebEngine from Qt 5.11 or newer
- Sorting columns in the JuK music player has been fixed
- Konsole renders box-drawing characters correctly again
