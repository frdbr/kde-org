---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE випущено збірку програм KDE 15.04.3
layout: application
title: KDE випущено збірку програм KDE 15.04.3
version: 15.04.3
---
1 липня 2015 року. Сьогодні KDE випущено третє стабільне оновлення <a href='../15.04.0'>набору програм KDE 15.04</a>. До цього випуску включено лише виправлення вад та оновлення перекладів. Його встановлення є безпечним і корисним для усіх користувачів.

Серед принаймні 20 зареєстрованих виправлень вад поліпшення у роботі kdenlive, kdepim, konsole, kopete, ktp-contact-list, marble, okteta та umbrello.

До цього випуску також включено версії з подовженою підтримкою робочих просторів Плазми 4.11.21, платформи для розробки KDE 4.14.10 та комплексу програм Kontact 4.14.10.
