---
date: 2013-08-14
hidden: true
title: KDE-sovellukset 4.11 tuo mukanaan suuria edistysaskelia henkilökohtaisten tietojen
  hallintaan ja parannuksia kaikkialle
---
The Dolphin file manager brings many small fixes and optimizations in this release. Loading large folders has been sped up and requires up to 30&#37; less memory. Heavy disk and CPU activity is prevented by only loading previews around the visible items. There have been many more improvements: for example, many bugs that affected expanded folders in Details View were fixed, no &quot;unknown&quot; placeholder icons will be shown any more when entering a folder, and middle clicking an archive now opens a new tab with the archive contents, creating a more consistent experience overall.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Kontactin uusi tapa sähköpostien lähettämiseen myöhemmin` width="600px">}}

## Parannuksia Kontact Suiteen

Kontact Suitessa on jälleen keskitytty vakauteen, suorituskykyyn ja muistinkulutukseen. Kansioiden tuontia, karttojen välillä vaihtamista, postin hakemista, merkitsemistä, suuren viestimäärän siirtämistä ja käynnistysaikaa on kaikkia parannettu viimeisen kuuden kuukauden aikana. Lue lisää <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>tästä blogista</a>. <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>Arkistointitoiminnallisuutta on korjailtu paljon</a>, ja myös opastettua tuontia on parannettu: asetukset voi nyt tuoda myös Trojitá-sähköpostiohjelmasta, ja muistakin ohjelmista tuonti toimii paremmin. Lisätietoja saat <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>täältä</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`Arkistointiohjelma huolehtii sähköpostien tallentamisesta pakattuun muotoon` width="600px">}}

This release also comes with some significant new features. There is a <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>new theme editor for email headers</a> and email images can be resized on the fly. The <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Send Later feature</a> allows scheduling the sending of emails on a specific date and time, with the added possibility of repeated sending according to a specified interval. KMail Sieve filter support (an IMAP feature allowing filtering on the server) has been improved, users can generate sieve filtering scripts <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>with an easy-to-use interface</a>. In the security area, KMail introduces automatic 'scam detection', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>showing a warning</a> when mails contain typical phishing tricks. You now receive an <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>informative notification</a> when new mail arrives. and last but not least, the Blogilo blog writer comes with a much-improved QtWebKit-based HTML editor.

## Katen laajempi kielituki

Kehittynyt tekstimuokkain Kate esittelee uusia liitännäisiä: Python (versiot 2 ja 3), JavaScript &JQuery, Django ja XML. Niissä on uutena ominaisuuksia kuten staattinen ja dynaaminen automaattitäydennys, syntaksin tarkistus, koodikatkelmien lisääminen sekä mahdollisuus sisentää XML:ää automaattisesti käyttäen pikanäppäintä. Pythonin ystäville on tarjolla vieläkin enemmän: python-konsoli, joka tarjoaa perusteellista tietoa avoinna olevasta lähdekooditiedostosta. Käyttöliittymässä on myös pieniä parannuksia, kuten <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>hakutoiminnallisuuden uusi passiivinen ilmoitus</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimoinnit VIM-tilaan</a> ja <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>uusi tekstin laskostustoiminnallisuus</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars näyttää tulevia mielenkiintoisia tapahtumia, jotka näkyvät sijainnistasi` width="600px">}}

## Muut parannukset sovelluksiin

Mitä peleihin ja opetusohjelmiin tulee, ne ovat saaneet useita pienempiä ja suurempia uusia ominaisuuksia sekä optimointeja. Kymmensormijärjestelmän harjoittelusta kiinnostuneet saattavat pitää KTouchin tuesta oikealta vasemmalle luettavalle tekstille siinä missä tähtien seuraajan ystävässä, KStarsissa, on nyt työkalu, joka näyttää tulevia mielenkiintoisia tapahtumia alueellasi. Matematiikkatyökalut Rocs, Kig, Cantor ja KAlgebra saivat kaikki huomiota, ja tukevat nyt enemmän taustajärjestelmiä sekä laskelmia. KJumpingCube-pelissä on nyt suurempia laudan kokoja, uusia taitotasoja, parempi käyttöliittymä, ja se vastaa nopeammin.

Yksinkertainen piirto-ohjelma Kolourpaint osaa käsitellä WebP-kuvamuotoa, ja yleisessä asiakirjakatselimessa Okularissa on muutettavissa olevat tarkastelutyökalut sekä tuki muutosten kumoamiselle ja uudelleen tekemiselle lomakkeiden ja merkintöjen osalta. JuK, musiikkisoitin ja tunnistetiedojen muokkain, tukee uuden Ogg Opus -äänimuodon toistoa ja sen metatietojen muokkausta, vaikkakin myös ääniajurin ja TagLibin tarvitsee tukea Ogg Opusta.

#### KDE-sovellusten asentaminen

KDE-ohjelmat, mukaan lukien kaikki KDE:n kirjastot ja sovellukset, ovat ilmaisesti saatavilla avoimen lähdekoodin lisenssien mukaisesti. KDE-ohjelmia voi käyttää useilla laitekokoonpanoilla ja suoritinarkkitehtuureilla kuten ARMilla ja x86:lla, useissa käyttöjärjestelmissä, ja ne toimivat kaikenlaisten ikkunointiohjelmien ja työpöytäympäristojen kanssa. Linux- ja UNIX-pohjaisten käyttöjärjestelmien lisäksi useimmista KDE-sovelluksista on saatavilla Microsoft Windows -versiot <a href='http://windows.kde.org'>KDE-ohjelmat Windowsissa</a> -sivustolta ja Apple Mac OS X -versiot <a href='http://mac.kde.org/'>KDE-ohjelmat Macillä</a> -sivustolta. Joistain KDE-sovelluksista on verkossa kokeellisia versioita, jotka toimivat useilla mobiilialustoilla kuten Meegolla, Microsoftin Windows Mobilella ja Symbianilla, mutta niitä ei tällä hetkellä tueta. <a href='http://plasma-active.org'>Plasma Active</a> on käyttökokemus laajemmalle laitekirjolle kuten esimerkiksi tablettitietokoneille ja muille mobiililaitteille.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paketit

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Pakettien sijainnit

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Järjestelmävaatimukset

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Tänään tiedotettiin myös seuraavista:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Valmistautuen pitkäaikaiseen ylläpitöön Plasma-työtilat parantaa perustoimintoja edelleen: julkaisussa on mukana jouhevampi tehtäväpalkki, älykkäämpi akkusovelma ja parannettu äänimikseri. KScreenin käyttöönotto tekee useiden näyttöjen hoitamisesta fiksumpaa, ja mittavat suorituskykyparannukset yhdessä pienten käytettävyysparannusten kanssa tekevät käyttökokemuksesta paremman.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

Tämä KDE-ohjelmistoalustan 4.11-julkaisu keskittyy edelleen vakauteen. Uusia ominaisuuksia toteutetaan parhaillaan myöhemmin tulossa olevaan KDE Frameworks 5.0 -julkaisuun, mutta onnistuimme mahduttamaan vakaaseen julkaisuumme optimointeja Nepomuk-sovelluskehykseemme.
