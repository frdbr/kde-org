---
aliases:
- ../../kde-frameworks-5.22.0
date: 2016-05-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Korralik kontroll, kas URL on kohalik fail

### Baloo

- Windowsis kompileerimise parandused

### Breeze'i ikoonid

- Palju uusi toimingute ja rakenduste ikoone.
- Pakutud laienduste määramine muutuste kaupa kiconthemes'is

### CMake'i lisamoodulid

- Androidi versioon: projektide toetamine ilma elementideta share'is või lib/qml-s (veateade 362578)
- KDE_INSTALL_USE_QT_SYS_PATHS lubamine, kui CMAKE_INSTALL_PREFIX on Qt5 prefiks
- ecm_qt_declare_logging_category: tõrketeate täiustamine kasutamisel ilma kaasamata

### Raamistike lõimimine

- platformtheme plugina eemaldamine, mis on nüüd plasma-integration'i all

### KCoreAddons

- inotify kasutamise keelamise võimaldamine KDirWatch'is
- KAboutData::applicationData() parandus initsialiseerimiseks aktiivse Q*Application'i metaandmetest
- Selgitamine, et KRandom ei ole krüptimiseks soovitatav

### KDBusAddons

- KDBusService: '-' asemel '_' objektide asukohas

### KDeclarative

- Krahhi vältimine, kui openGL kontekst puudub

### KDELibs 4 toetus

- Defineerimata jätmisel varuvariandi MAXPATHLEN pakkumine
- KDateTime::isValid() parandus ClockTime'i väärtustes (veateade 336738)

### KDocTools

- Rakenduste olemite lisamine

### KFileMetaData

- Haru 'externalextractors' ühendamine
- Väliste pluginate ja testide parandus
- Väliste kirjutamispluginate toetuse lisamine
- Kirjutamisplugina toetuse lisamine
- Välise ekstraktimisplugina toetuse lisamine

### KHTML

- toString'i teostus Uint8ArrayConstructor'ile ja semudele
- Mitme Coverity'ga seonduva paranduse ühendamine
- QCache::insert'i korrektne kasutamine
- Mõne mälulekke parandus
- CSS veebifondi parsimise tervisekontroll, võimaliku mälulekke vältimine
- dom: sildi prioriteetide lisamine sildile 'comment'

### KI18n

- libgettext: võimaliku use-after-free parandus muudes kui g++ kompilaatorites

### KIconThemes

- Sobiva konteineri kasutamine sisemisele viidamassiivile
- Võimaluse lisamine vähendada tarbetuid kettapöördusi, KDE Extensions'i lisamine
- Mõne kettapöörduse salvestamine

### KIO

- kurlnavigatortoolbutton.cpp - buttonWidth'i kasutamine paintEvent()-s
- Uus failimenüü: duplikaatide väljafiltreerimine (nt qrc ja süsteemsete failide vahel) (veateade 353390)
- Tõrketeate parandus küpsiste juhtimismooduli käivitamisel
- kmailservice5 eemaldamine, mis praegu tekitab ainult kahju (veateade 354151)
- KFileItem::refresh() parandus nimeviitade korral. Määrati vale suurus, failitüüp ja õigused
- KFileItem: refresh() tagasilanguse parandus, mis kaotas failitüübi, nii et kataloog muutus failiks (veateade 353195)
- Teksti määramine QCheckbox'i vidinale eraldi pealdise kasutamise asemel (veateade 245580)
- Vidinal ei lubata määrata acl õigusi, kui me ei ole faili omanik (veateade 245580)
- Topeltkaldriipsu parandus KUriFilter'i tulemustes, kui määratud on nimefilter
- KUrlRequester: signaali textEdited lisamine (edastatud QLineEdit'ist)

### KItemModels

- Testjuhtumi genereerimisel malli süntaksi parandus
- Linkimise parandus Qt 5.4-ga (valesti asetatud #endif)

### KParts

- BrowserOpenOrSaveQuestion dialoogi paigutuse parandus

### KPeople

- PersonData kehtivuse kontrolli lisamine

### KRunner

- metainfo.yaml'i parandus: KRunner ei ole portimisabiline ega iganenud

### KService

- Liiga range stringi maksimaalse pikkuse piirangu eemaldamine KSycoca andmebaasis

### KTextEditor

- Use proper char syntax '"' instead of '"'
- doxygen.xml: vaikestiili  dsAnnotation kasutamine ka "kohandatud siltide" korral (vähem otse koodi kirjutatud värve)
- Valiku lisamine lasta näidata sõnade loendurit
- Esiplaani värvi kontrasti täiustamine otsimise ja asendamise esiletõstmistes
- Krahhi vältimine Kate sulgemisel dbus'i abil, kui trükkimisdialoog on avatud (veateade 356813)
- Cursor::isValid(): märkuse lisamine isValidTextPosition() kohta
- {Cursor, Range}::{toString, static fromString} API lisamine

### KUnitConversion

- Kliendi teavitamine, kui vahetuskurss pole teada
- Vääringu ILS (Iisraeli uues seekel) lisamine (veateade 336016)

### KWalleti raamistik

- kwalletd5 seansi taastamise keelamine

### KWidgetsAddons

- KNewPasswordWidget: suurusevihje eemaldamine eraldajal, mis tekitas mõnikord paigutuses alati tühjaks jääva ala
- KNewPasswordWidget: QPalette'i parandus vidina keelamisel

### KWindowSystem

- xcb plugina asukoha genereerimise parandus

### Plasma raamistik

- [QuickTheme] omaduste parandus
- highlight/highlightedText sobivast värvirühmast
- ConfigModel: allika tühja asukohta ei lahendata paketi põhjal
- [kalender] ainult kuupäeva-, mitte kuu- või aastavaates sündmuste tähise näitamine
- declarativeimports/core/windowthumbnail.h - -Wreorder hoiatuse parandus
- ikooniteema kohane taaslaadimine
- Teema nimi kirjutatakse alati faili plasmarc, isegi kui valitud on vaiketeema
- [kalender] tähise lisamine sündmusega päevade juurde
- positiivse, neutraalse, negatiivse tekstivärvi lisamine
- ScrollArea: hoiatuse parandus, kui contentItem ei ole Flickable
- Omaduse ja meetodi lisamine menüü joondamiseks visuaalse eellase nurgaga
- Menüü miinimumlaiuse määramise lubamine
- Salvestatud elementide loendi järjekorra säilitamine
- API laiendamine lubamaks menüüelementide (ümber)paigutamist protseduurilise lisamise käigus
- highlightedText'i värvi seondamine Plasma::Theme'is
- Plasma aplettide seonduvate rakenduste/URL-ide määramise tühistamise parandus
- Privaatklassi DataEngineManager sümboleid ei avalikustata
- Elemendi "event" lisamine calendar.svg-sse
- SortFilterModel: filtri väljakutse muutmisel filtri kehtivuse tühistamine

### Sonnet

- Tööriista parsetrigrams paigaldamine ristkompileerimise võimaldamiseks
- hunspell: isikliku sõnastiku laadimine/salvestamine
- Hunspell 1.4 toetus
- configwidget: märguanne muudetud seadistuse kohta, kui eiratavaid sõnu uuendati
- seadistused: seadistust ei salvestata kohe eiratavate sõnade loendi uuendamisel
- configwidget: salvestamise parandus eiratavate sõnade uuendamisel
- Eiratavate sõnade salvestamise nurjumise tõrke parandus (veateade 355793)

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
