---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE rilascia KDE Applications 18.04.3
layout: application
title: KDE rilascia KDE Applications 18.04.3
version: 18.04.3
---
12 luglio 2018. Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../18.04.0'>KDE Applications 18.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Circa venti errori corretti includono, tra gli altri, miglioramenti a Kontact, Ark, Cantor, Dolphin, Gwenview e KMag.

I miglioramenti includono:

- È stata ripristinata la compatibilità con i server IMAP che non annunciano le loro capacità
- Ark ora è in grado di estrarre gli archivi ZIP in cui mancano le voci corrette per le cartelle
- Le note sullo schermo di KNotes seguono di nuovo il puntatore del mouse durante gli spostamenti
