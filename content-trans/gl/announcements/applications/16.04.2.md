---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE publica a versión 16.04.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.04.2 das aplicacións de KDE
version: 16.04.2
---
June 14, 2016. Today KDE released the second stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 25 correccións de erros inclúen melloras en, entre outros, Akonadi, Ark, Artikulate, Dolphin, Kdenlive e KDE PIM.

This release also includes Long Term Support version of KDE Development Platform 4.14.21.
