---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE Ships Applications 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE publica a versión 19.04.3 das aplicacións de KDE
version: 19.04.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Máis de sesenta correccións de erros inclúen melloras en, entre outros, Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular e Umbrello.

Entre as melloras están:

- Konqueror e Kontact xa non quebran ao saír con QtWebEngine 5.13
- Cortar grupos con composicións xa non quebra o editor de vídeo Kdenlive
- O importador de Python no deseñador de UML Umbrello xa xestiona parámetros con argumentos predeterminados
