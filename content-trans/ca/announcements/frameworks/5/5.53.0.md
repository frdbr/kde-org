---
aliases:
- ../../kde-frameworks-5.53.0
date: 2018-12-09
layout: framework
libCount: 70
---
### Baloo

- Corregeix cerques per puntuacions de 10 (5 estrelles) (error 357960)
- Evita escriure dades sense canvis a la BBDD de termes
- No afegeix dues vegades Type::Document/Presentation/Spreadsheet per als documents del MS Office
- Inicialitza adequadament «kcrash» realment
- S'assegura que només hi ha un MTime per document al MTimeDB
- [Extractor] Usa la serialització del QDataStream en lloc de cuinar-ne una
- [Extractor] Substitueix el gestor IO casolà per al «QDataStream», tractament de HUP

### Icones Brisa

- Afegeix icones per «application-vnd.appimage»/«x-iso9660-appimage»
- Afegeix la icona «dialog-warning» de 22px (error 397983)
- Soluciona l'angle i el marge de «dialog-ok-apply» de 32px (error 393608)
- Canvia els colors primaris de les icones monocromes per coincidir amb els colors nous del HIG
- Canvia les icones «archive-*» d'acció per representar arxius (error 399253)
- Afegeix un enllaç simbòlic de «help-browser» als directoris de 16px i 22px (error 400852)
- Afegeix icones noves genèriques d'ordenació; reanomena les icones existents d'ordenació
- Afegeix la versió de l'arrel de «drive-harddisk» (error 399307)

### Mòduls extres del CMake

- Mòdul nou: FindLibExiv2.cmake

### Integració del marc de treball

- Afegeix l'opció BUILD_KPACKAGE_INSTALL_HANDLERS per ometre la construcció dels gestors d'instal·lació

### Eines de Doxygen del KDE

- Afegeix un indicador d'ocupat durant la cerca i fa que aquesta sigui asíncrona (error 379281)
- Normalitza tots els camins d'entrada amb la funció «os.path.normpath» (error 392428)

### KCMUtils

- Alineació perfecta entre els títols del KCM escrits en QML i QWidget
- Afegeix el context a la connexió del «kcmodule» amb les lambdes (error 397894)

### KCoreAddons

- Fa possible usar KAboutData/License/Person des del QML
- Soluciona la fallada si el directori XDG_CACHE_HOME és massa petit o no hi ha espai (error 339829)

### KDE DNS-SD

- Ara instal·la «kdnssd_version.h» per comprovar la versió de la biblioteca
- Evita fuites del resolutor al «remoteservice»
- Evita una situació de competició del senyal Avahi
- Esmena per a macOS

### KFileMetaData

- Reactivació de la propietat «Description» per a les metadades del DublinCore
- Afegeix una propietat de descripció a KFileMetaData
- [KFileMetaData] Afegeix un extractor per al Postscript (encapsulat) conforme al DSC
- [ExtractorCollection] Evita la dependència dels «kcoreaddons» per a les proves de CI
- Afegeix la implementació per als fitxers «speex» al «taglibextractor»
- Afegeix dues fonts més d'Internet relacionades amb la informació d'etiquetatge
- Simplifica la gestió d'etiquetes ID3
- [XmlExtractor] Usa QXmlStreamReader per a un millor rendiment

### KIO

- Esmena una asserció en netejar els enllaços simbòlics a PreviewJob
- Afegeix la possibilitat de tenir una drecera de teclat per crear un fitxer
- [KUrlNavigator] Es reactiva en clicar amb el botó del mig del ratolí (error 386453)
- Elimina els proveïdors de cerca morts
- Adaptar més proveïdors de cerques a HTTPS
- Torna a exportar KFilePlaceEditDialog (error 376619)
- Restaura la implementació del «sendfile»
- [ioslaves/trash] Gestiona els enllaços simbòlics trencats en subdirectoris suprimits (error 400990)
- [RenameDialog] Esmena la disposició en usar l'indicador NoRename
- Afegeix un «@since» que mancava a KFilePlacesModel::TagsType
- [KDirOperator] Usa la icona <code>view-sort</code> nova per triar el sentit de l'ordenació
- Usa HTTPS per a tots els proveïdors de cerques que l'admeten
- Desactiva l'opció de desmuntar per / o /home (error 399659)
- [KFilePlaceEditDialog] Esmena una protecció d'inclusió
- [Places panel] Usa la icona <code>folder-root</code> nova per l'element Arrel
- [KSambaShare] Fa que «net usershare info» es pugui analitzar
- Aporta el botó de menú «Ordena per» als diàlegs de fitxer a la barra d'eines

### Kirigami

- DelegateRecycler: No crea un «propertiesTracker» nou per a cada delegat
- Mou la pàgina «Quant a» del Discover al Kirigami
- Oculta el calaix de context quan hi ha una barra d'eines global
- Assegura que es disposen tots els elements (error 400671)
- Canvia l'índex en prémer, no en fer clic (error 400518)
- Mides noves de text per a les capçaleres
- Els calaixos de les barres laterals no mouen les capçaleres/peus de pàgina globals

### KNewStuff

- Afegeix per programa senyals d'error més útils

### KNotification

- Reanomena NotifyByFlatpak a NotifyByPortal
- Portal de notificacions: permet mapes de píxels a les notificacions

### Framework del KPackage

- No genera dades «appstream» per a fitxers que tenen manca de descripció (error 400431)
- Captura les metadades del paquet abans d'iniciar la instal·lació

### KRunner

- Quan es reutilitzen executors en recarregar, recarrega la seva configuració (error 399621)

### KTextEditor

- Permet prioritats negatives de definició de sintaxi
- Exposa la funcionalitat «Commuta els comentaris» al menú d'eines i a la drecera predeterminada (error 387654)
- Soluciona els idiomes ocults al menú de mode
- SpellCheckBar: Usa DictionaryComboBox en lloc d'una QComboBox simple
- KTextEditor::ViewPrivate: Evita l'avís «Text requerit per a un interval no vàlid»
- Android: Ja no cal definir més «log2»
- Desconnecta el menú contextual de tots els receptors «aboutToXXContextMenu» (error 401069)
- Presenta AbstractAnnotationItemDelegate per a més control del consumidor

### KUnitConversion

- S'ha actualitzat amb les unitats industrials del petroli (error 388074)

### KWayland

- Genera automàticament el fitxer de registre + el fitxer de categories esmenades
- Afegeix VirtualDesktops a PlasmaWindowModel
- Actualitza la prova del PlasmaWindowModel per reflectir els canvis al VirtualDesktop
- Neteja «windowInterface» a les proves abans que es destrueixi «windowManagement»
- Suprimeix l'element correcte a «removeDesktop»
- Neteja l'entrada de la llista del Gestor d'escriptoris virtuals al destructor del PlasmaVirtualDesktop
- Versió correcta de la nova interfície afegida PlasmaVirtualDesktop
- [server] Consell del contingut del text d'entrada i finalitat per versió de protocol
- [server] Posa crides de retorn d'entrada de text «(de-)activate, en-/disable» a les classes filles
- [server] Posa rutines de retorn de text circumdant amb «uint» a la classe v0
- [server] Posa diverses crides de retorn exclusives de v0 d'entrada de text a la classe v0

### KWidgetsAddons

- Afegeix el nivell de l'API des del Kirigami.Heading

### KXMLGUI

- Actualitza el text «Quant al KDE»

### NetworkManagerQt

- Soluciona un error(?) als paràmetres IPv4 &amp; IPv6
- Afegeix les opcions «ovs-bridge» i «ovs-interface»
- Actualitza els paràmetres d'IpTunnel
- Afegeix la configuració per servidor intermediari i usuari
- Afegeix la configuració per IpTunnel
- Ara es pot construir un prova d'establiment «tun» en qualsevol moment
- Afegeix les opcions IPv6 que mancaven
- Escolta les interfícies D-Bus afegides en lloc dels serveis enregistrats (error 400359)

### Frameworks del Plasma

- Funcionalitat de paritat del Menú amb l'estil de l'escriptori
- Qt = 5.9 ara és la versió mínima requerida
- Retorna una línia suprimida (accidentalment?) al CMakeLists.txt
- 100% coherència amb la mida de la capçalera del Kirigami
- Aspecte més homogeni amb les capçaleres del Kirigami
- Instal·la la versió processada de les importacions privades
- Controls de selecció de text mòbil
- Actualitza els esquemes de color Brisa-clar i Brisa-fosc
- Soluciona diverses fuites de memòria (gràcies a l'ASAN)

### Purpose

- Connector del Phabricator: usa l'ordre del «diff.rev» de l'Arcanist (error 401565)
- Proporciona un títol per JobDialog
- Permet que el JobDialog aconsegueixi una mida inicial més agradable (error 400873)
- Fer possible per al «menudemo» compartir URL diferents
- Usa el QQC2 per al JobDialog (error 400997)

### QQC2StyleBridge

- Mida coherent de l'element comparat amb els QWidgets
- Soluciona la mida del menú
- S'assegura que els «flickables» estiguin «pixelAligned»
- Implementació per a les aplicacions basades en QGuiApplication (error 396287)
- Controls de text de pantalla tàctil
- Mida d'acord amb l'amplada i alçada especificada de la icona
- Respecta la propietat «flat» dels botons
- Soluciona el problema quan només hi ha un element al menú (error 400517)

### Solid

- Esmena el canvi d'icona del disc arrel, de manera que no calgui canviar erròniament altres icones

### Sonnet

- DictionaryComboBoxTest: Afegeix un estirament per evitar expandir el botó «Bolcat»

### Ressaltat de la sintaxi

- BrightScript: Permetre un «sub» sense nom
- Afegeix un fitxer de ressaltat per a les Wayland Debug Traces
- Afegeix el ressaltat de sintaxi per al TypeScript i el TypeScript React
- Rust &amp; Yacc/Bison: millora els comentaris
- Prolog &amp; Lua: esmena el «shebang»
- Soluciona la càrrega del llenguatge després d'incloure paraules clau d'aquest llenguatge en un altre fitxer
- Afegeix la sintaxi del BrightScript
- Debchangelog: afegeix Disco Dingo

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
