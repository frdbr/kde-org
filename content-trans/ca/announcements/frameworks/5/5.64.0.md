---
aliases:
- ../../kde-frameworks-5.64.0
date: 2019-11-10
layout: framework
libCount: 70
---
### Attica

- Afegeix diverses «std::move» a les funcions «setter»

### Baloo

- Fa que compili amb les Qt 5.15
- Usa «propertymap» per emmagatzemar les propietats a Baloo::Result
- Afegeix funcions de conversió autònomes de PropertyMap a Json i viceversa
- [Database] Reescriu la gestió dels indicadors d'entorn
- Substitueix una recursió a FilteredDirIterator amb una iteració de bucle

### Icones Brisa

- Alinea al centre les icones de tipus MIME d'àudio de 64px no quadrades (error 393550)
- Suprimeix els romanents de les icones del Nepomuk
- Mou la icona «help-about» a tot color de 32px a les accions (error 396626)
- Millora les icones de dibuix (error 399665)
- Suprimeix la icona del Nepomuk
- Omple l'àrea del botó del mig del ratolí
- Afegeix «folder-recent», amplia la maneta del rellotge a «folder-temp»
- Usa una metàfora visual més correcta i apropiada per a la icona «get hot new stuff» (error 400500)
- Actualitza la icona de l'Elisa
- Usa CSS en lloc de SCSS com a format de sortida
- Corregeix la representació incorrecta de la icona «edit-opacity» de 22px
- Afegeix icones «edit-opacity» (error 408283)
- Icones per a temps ventós (error 412718)
- Corregeix els marges incorrectes a les icones «media» de 16/22px
- Usa el text abans que el color de ressaltat per als emblemes de valoracions/estrelles
- Afegeix icones «draw-arrow» (error 408283)
- Afegeix icones d'acció «draw-highlight» (error 408283)
- Afegeix PATH/LD_LIBRARY_PATH a la invocació de «qrcAlias»
- Afegeix la icona «applications-network» per reanomenar la categoria Internet a Xarxa
- Afegeix icones «edit-line-width» (error 408283)

### Mòduls extres del CMake

- No estableix els estàndards del C/C++ si ja s'han establert
- Usa la manera moderna d'establir l'estàndard del C/CXX
- Eleva els requisits del CMake a 3.5
- ECMAddQch: admet PREDEFINED_MACROS/BLANK_MACROS amb blancs i cometes

### Integració del marc de treball

- Afegeix les icones estàndard per admetre totes les entrades al QDialogButtonBox (error 398973)
- Assegura que «winId()» no es crida en ginys no natius (error 412675)

### KActivitiesStats

- Proves: corregeix la fallada de construcció a macOS
- Esmenes de compilació en el MSVC del Windows
- Afegeix un mètode d'accés utilitari per obtenir un QUrl a partir d'un ResultSet::Result

### KArchive

- Corregeix una fuita de memòria a KXzFilter::init
- Corregeix una referència a un apuntador nul en fallar l'extracció
- decodeBCJ2: Corregeix una asserció amb fitxers trencats
- KXzFilter::Private: elimina propietats no usades
- K7Zip: corregeix l'ús de la memòria a «readAndDecodePackedStreams»

### KCalendarCore

- També afegeix la versió de la «libical»
- Defineix explícitament el «ctor» de còpia del Diari

### KCMUtils

- Mostra condicionalment els botons de navegació a la capçalera als KCM amb multipàgina
- No usa una alçada personalitzada de capçalera (error 404396)
- Afegeix una inclusió extra
- Corregeix una fuita de memòria dels objectes de KQuickAddons::ConfigModule (error 412998)
- [KCModuleLoader] Mostra un error quan el QML falla en carregar

### KConfig

- kconfig_compiler: Mou els KSharedConfig::Ptr quan s'usin
- Fa que compili amb les Qt 5.15 sense un mètode obsolet
- Exposa «isImmutable» a introspecció (p. ex. QML)
- Afegeix una utilitat per als estats «defaults»/«dirty» al KCoreConfigSkeleton
- Fa que «kconfig_compiler» generi «ctors» amb l'argument pare opcional
- Fa que «preferences()» sigui una funció pública

### KConfigWidgets

- Evita la sobrecàrrega de «KCModule::changed»

### KContacts

- Instal·lació de traduccions

### KCoreAddons

- KProcessInfoList -- afegeix un dorsal de «proclist» per al FreeBSD

### KDeclarative

- Una un «connect» comprovat en temps de compilació
- Converteix en protegit el sòcol «settingChanged()»
- Fa que KQuickAddons::ConfigModule exposi si s'està en l'estat predeterminat
- Captura el teclat quan «KeySequenceItem» està enregistrant
- Afegeix ManagedConfigModule
- Elimina el comentari obsolet quant a l'expansió de [$e]
- [ConfigModule] Exposa l'estat del component «mainUi» i la cadena d'error

### Compatibilitat amb les KDELibs 4

- KLocale API docs: facilita trobar com adaptar el codi per eliminar-ho

### KDocTools

- man: usa &lt;arg&gt; en lloc de &lt;group&gt;

### KFileMetaData

- Esmenar una fallada a la recol·lecció i neteja de l'escriptor

### KHTML

- Amplia KHtmlView::print() per usar una instància predefinida de QPrinter (error 405011)

### KI18n

- Afegeix KLocalizedString::untranslatedText
- Substitueix tots els «qWarning» i les crides relacionades amb un enregistrament categoritzat

### KIconThemes

- Corregeix l'ús de les noves macros d'obsolescència per «assignIconsToContextMenu»
- Fa obsolet KIconTheme::assignIconsToContextMenu

### KIO

- «Const» i signatura del nou SlaveBase::configValue presentat
- Adapta a la variant QSslError del KSslInfoDialog
- Adaptacions internes del KSSLD des del KSslError al QSslError
- Fa explícits els errors no ignorables del SSL
- Activa automàticament KIO_ASSERT_SLAVE_STATES també per a les construccions des del Git
- Adapta (la majoria de) KSslInfoDialog des del KSslError al QSslError
- kio_http: evita el doble «Content-Type» i la «Depth» en ser usat pel KDAV
- Adapta la interfície D-Bus del KSSLD des del KSslError al QSslError
- Substitueix l'ús de SlaveBase::config()-&gt;readEntry amb SlaveBase::configValue
- Elimina dues variables de membres sense ús usant el KSslError
- Evita enviar KDirNotify::emitFilesAdded en acabar l'«emptytrashjob»
- Fa obsoleta la variant basada en el KTcpSocket del SslUi::askIgnoreSslErrors
- Tracta «application/x-ms-dos-executable» com a un executable a totes les plataformes (error 412694)
- Substitueix l'ús de SlaveBase::config() amb SlaveBase::mapConfig()
- ftptest: substitueix «logger-colors» amb «logger»
- [SlaveBase] Usa QMap en lloc de KConfig per emmagatzemar la configuració de l'«ioslave»
- Adapta KSslErrorUiData a QSslError
- Exclou el directori dels «ioslaves» de la documentació de l'API
- ftptest: ja no falla la sobreescriptura de marca sense l'indicador de sobreescriptura
- ftptest: refactoritza l'inici del dimoni dins la seva pròpia funció «helper»
- [SslUi] Afegeix la documentació de l'API per «askIgnoreSslErrors()»
- Considera com no requerir el «ftpd» per als temps que corren
- Adapta l'esclau «ftp» al nou sistema d'informes d'error
- Corregeix la càrrega de les opcions del servidor intermediari
- Implementa la KSslCertificateRule amb el QSslError en lloc del KSslError
- Adapta (la gran part) de la interfície del KSslCertificateRule al QSslError
- Adapta el KSslCertificateManager al QSslError
- Actualitza la prova «kfileplacesviewtest» seguint la D7446

### Kirigami

- Assegura que el «topContent» del GlobalDrawer sempre està a la part superior (error 389533)
- Ressalta en passar per sobre el ratolí només en el mode d'una pàgina (error 410673)
- Reanomena l'Okular Active a l'Okular Mobile
- Els elements tenen el focus actiu a la pestanya quan no estan a una vista (error 407524)
- Permet que les «contextualActions» flueixin a la barra d'eines de la capçalera
- Corregeix el model Credits incorrecte de la Kirigami.AboutPage
- No mostra el calaix de context si totes les accions són invisibles
- Corregeix la imatge de plantilla del Kirigami
- Manté els contenidors desproveïts dels elements suprimits
- Limita la mida dels marges d'arrossegament
- Corregeix la visualització del botó d'eina del menú quan no hi ha cap calaix disponible
- Desactiva el calaix global d'arrossegament en el mode menú
- Mostra text del consell d'eina dels elements del menú
- No avisa quant a la LayoutDirection al SearchField
- Comprova adequadament l'estat activat de l'acció per als botons ActionToolBar
- Usa la propietat de l'acció del MenuItem directament a ActionMenuItem
- Permet que el calaix global esdevingui un menú si es desitja
- És més explícit quant als tipus de propietat d'acció

### KItemViews

- [RFC] Unifica l'estil del Kirigami.ListSectionHeader nou i el CategoryDrawer

### KJS

- Missatge millor per als errors d'interval a String.prototype.repeat(count)
- Simplifica l'anàlisi dels literals numèrics
- Analitza els literals binaris en JS
- Detecta els literals hexadecimals i octals truncats
- Admet la nova manera estàndard d'especificar literals octals
- Col·lecció de proves de regressió preses del repositori «khtmltests»

### KNewStuff

- Assegura que la propietat «changedEntries» es propaga correctament
- Corregeix la recuperació de KNSCore::Cache en inicialitzar Engine (error 408716)

### KNotification

- [KStatusNotifierItem] Permet el clic esquerre quan el menú és nul (error 365105)
- Elimina el suport del Growl
- Afegeix i activa la implementació del Centre de notificacions a macOS

### KPeople

- Soluciona la construcció: limita DISABLE_DEPRECATED per al KService a &lt; 5.0

### KService

- Fa que compili amb les Qt 5.15 sense un mètode obsolet

### KTextEditor

- KateModeMenuList: millora l'ajust de paraules
- Corregeix una fallada (error 413474)
- Han arribat més conformitats, més v2+
- Han arribat més conformitats, més v2+
- Afegeix un consell a la capçalera del Copyright
- Les línies de codi no trivials no resten aquí excepte per a persones que estan conformes amb v2+ =&gt; v2+
- Les línies de codi no trivials resten aquí dels autors que encara no han contestat, v2+
- Més fitxers amb v2+, ja que s'ha aconseguit l'acord dels autors, vegeu kwrite-devel@kde.org
- Més fitxers amb v2+, ja que s'ha aconseguit l'acord dels autors, vegeu kwrite-devel@kde.org
- Més fitxers amb v2+, ja que s'ha aconseguit l'acord dels autors, vegeu kwrite-devel@kde.org
- Més fitxers amb v2+, ja que s'ha aconseguit l'acord dels autors, vegeu kwrite-devel@kde.org
- Més fitxers amb v2+, ja que s'ha aconseguit l'acord dels autors, vegeu kwrite-devel@kde.org
- Més fitxers amb v2+, ja que s'ha aconseguit l'acord dels autors, vegeu kwrite-devel@kde.org
- «katedocument.h» ja és v2+
- Conformitat per «loh.tar», «sars», LGPLv2+
- Torna a llicenciar a LGPLv2+, amb la conformitat de l'Sven + Michal
- Torna a llicenciar a LGPLv2+, amb la conformitat de l'Sven + Michal
- Tots els fitxers amb SPDX-License-Identifier són LGPLv2+
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Actualitza la llicència, «dh» és a la secció de la LGPLv2+ de «relicensecheck.pl»
- Actualitza la llicència, «dh» és a la secció de la LGPLv2+ de «relicensecheck.pl»
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- LGPLv2.1+ =&gt; LGPLv2+
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Aquesta capçalera conté no només GPL v2 lògica, ja que és llarga
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, afegeix el SPDX-License-Identifier
- Clarifica la llicència, «michalhumpula» =&gt; [«GPLv23», «LGPLv23», «GPLv2+», «LGPLv2+», «+eV» ]
- Afegeix una «s» que manca (error 413158)
- KateModeMenuList: força la posició vertical sobre el botó
- Millora: capçaleres autocontingudes
- Agrupa les inclusions segons la semàntica
- Ordena les inclusions

### KTextWidgets

- Elimina una crida que ja no cal KIconTheme::assignIconsToContextMenu

### KWayland

- FakeInput: afegeix la implementació per prémer i deixar anar una tecla del teclat
- Corregeix una còpia d'escala no entera a la creació d'OutputChangeSet

### KXMLGUI

- Corregeix la detecció de drecera predeterminada

### NetworkManagerQt

- Afegeix la implementació per a l'autenticació SAE usada per a WPA3

### Frameworks del Plasma

- Mapa «disabledTextColor» a ColorScope
- Afegeix DisabledTextColor a Theme
- [PC3/button] Elideix sempre el text
- Millora les entrades del menú de les opcions del plafó
- [icons/media.svg] Afegeix icones de 16 i 32px, actualitza l'estil
- [PlasmaComponents3] Corregeix el fons del botó d'eina activable

### Prison

- Corregeix la gestió de la memòria al «datamatrix»

### Purpose

- i18n: Afegeix el·lipsis als elements d'acció (X-Purpose-ActionDisplay)

### QQC2StyleBridge

- No assigna «currentIndex» un quadre combinat, ja que trenca la vinculació
- Escolta el canvi d'estil de l'aplicació

### Solid

- No construeix la biblioteca estàtica quan BUILD_TESTING=OFF

### Ressaltat de la sintaxi

- VHDL: totes les paraules clau no distingeixen entre majúscules i minúscules (error 413409)
- Afegeix caràcters d'escapada de cadena a la sintaxi del PowerShell
- Modelines: corregeix el final de comentari
- Meson: més funcions integrades i afegeix funcions de membres integrats
- Debchangelog: afegeix Focal Fossa
- Actualitzacions del CMake 3.16
- Meson: afegeix una secció de comentari per comentar/descomentar amb el Kate
- TypeScript: actualitza la gramàtica i correccions

### ThreadWeaver

- Fa que compili amb les Qt 5.15

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
