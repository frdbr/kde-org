---
aliases:
- ../../kde-frameworks-5.33.0
date: 2017-04-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- S'ha afegit la descripció per a les ordres (balooctl)
- També cerca als directoris enllaçats dinàmicament (error 333678)

### BluezQt

- Proporciona el tipus de dispositiu per als dispositius de baix consum

### Mòduls extres del CMake

- Especifica «qml-root-path» com a directori compartit en el prefix
- Soluciona la compatibilitat de «ecm_generate_pkgconfig_file» amb el CMake nou
- Registra només les opcions APPLE_* «if(APPLE)»

### KActivitiesStats

- S'han afegit predefinicions a l'aplicació de proves
- Mou adequadament els elements a la posició desitjada
- Sincronitza la reordenació amb altres instàncies dels models
- Si l'ordre no està definit, ordena les entrades pel seu ID

### Eines de Doxygen del KDE

- [Meta] Canvi del mantenidor a «setup.py»

### KAuth

- Dorsal per al Mac
- Afegeix la implementació per matar un KAuth::ExecuteJob

### KConfig

- Saneja la llista de dreceres en llegir/escriure des de «kdeglobals»
- Evita reassignacions inútils eliminant una crida «squeeze» en la memòria intermèdia temporal

### KDBusAddons

- KDBusService: Afegeix un mètode d'accés per al nom del servei D-Bus que s'ha registrat

### KDeclarative

- Amb les Qt &gt;= 5.8 usa l'API nova per definir el dorsal del gràfic de l'escena
- No estableix cap «acceptHoverEvents» a la «DragArea», ja que no s'usa

### KDocTools

- Meinproc5: enllaça als fitxers, no a la biblioteca (error 377406)

### KFileMetaData

- Fa que «PlainTextExtractor» torni a quadrar amb «text/plain»

### KHTML

- Pàgina d'error, carrega correctament la imatge (amb un URL autèntic)

### KIO

- Fa que la redirecció de l'URL remot file:// a smb://torni a funcionar
- Manté la codificació de la consulta quan s'usa un intermediari HTTP
- Agents d'usuari actualitzats (Firefox 52 ESR, Chromium 57)
- Gestiona/trunca la cadena mostrada de l'URL assignada a la descripció d'un treball. Evita dades grans: que els URL s'incloguin a les notificacions de la IU
- Afegeix KFileWidget::setSelectedUrl() (error 376365)
- Esmena el mode de desar «KUrlRequester» afegint «setAcceptMode»

### KItemModels

- Menciona el nou QSFPM::setRecursiveFiltering(true) que fa obsolet el «KRecursiveFilterProxyModel»

### KNotification

- No elimina les notificacions de la cua quan s'inicia el servei «fd.o»
- Adaptacions a la plataforma Mac

### KParts

- API dox: soluciona la nota que manca en cridar «setXMLFile» amb KParts::MainWindow

### KService

- Soluciona els missatges de terminal «No s'ha trobat: ""»

### KTextEditor

- Exposa la funcionalitat addicional interna del View a l'API pública
- Desa força assignació per al «setPen»
- Soluciona ConfigInterface del KTextEditor::Document
- S'han afegit les opcions tipus de lletra i verificació ortogràfica al vol a ConfigInterface

### KWayland

- Afegeix la implementació per a «wl_shell_surface::set_popup» i «popup_done»

### KWidgetsAddons

- Implementació per construir amb les Qt sense «a11y» habilitat
- Esmena la mida incorrecta del consell quan es crida «animatedShow» amb un pare ocult (error 377676)
- Esmena els caràcters a KCharSelectTable que s'elideixen
- Activa tots els plans en el diàleg de proves del Kcharselect

### NetworkManagerQt

- WiredSetting: retorna negociació automàtica encara que estigui desactivada
- Evita que els senyals a «glib2» es defineixen a les Qt
- WiredSetting: Velocitat i dúplex només es poden definir quan la negociació automàtica està inactiva (error 376018)
- El valor de la negociació automàtica per l'opció de xarxa per cable ha de ser fals

### Frameworks del Plasma

- [ModelContextMenu] Usa un instanciador en lloc d'un pedaç per repetir i tornar a emparentar
- [Calendari] Encongeix i elideix els noms de les setmanes com es fa amb el delegat del dia (error 378020)
- [Icon Item] Fa que la propietat «smooth» realment faci quelcom
- Defineix una mida implícita a partir de la mida original per a les fonts d'URL per «image/SVG»
- Afegeix una propietat nova en els contenidors, per al mode d'edició
- Corregeix «maskRequestedPrefix» quan no s'usa cap prefix (error 377893)
- [Menu] Harmonitza la col·locació de l'«openRelative»
- Ara la majoria dels menús (contextuals) tenen acceleradors (Alt+lletra de les dreceres) (error 361915)
- Controls del Plasma basats en els QtQuickControls2
- Gestió d'«applyPrefixes» amb una cadena buida (error 377441)
- Elimina realment la memòria cau dels temes antics
- [Containment Interface] Activa el menú contextual en prémer la tecla «Menú»
- [Breeze Plasma Theme] Millora les icones de recobriment d'acció (error 376321)

### Ressaltat de la sintaxi

- TOML: Soluciona el ressaltat de les cadenes de les seqüències d'escapada
- Actualitza el ressaltat de la sintaxi del Clojure
- Diverses actualitzacions a la sintaxi de l'OCaml
- Ressalta els fitxers *.sbt com a codi del Scala
- Usa també el ressaltat de QML per als fitxers «.qmltypes»

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
