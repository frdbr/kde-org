---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၄.၃ တင်ပို့ခဲ့သည်
layout: application
title: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၄.၃ တင်ပို့ခဲ့သည်
version: 18.04.3
---
July 12, 2018. Today KDE released the third stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About 20 recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Gwenview, KMag, among others.

တိုးတက်မှုများ -

- Compatibility with IMAP servers that do not announce their capabilities has been restored
- Ark can now extract ZIP archives which lack proper entries for folders
- KNotes on-screen notes again follow the mouse pointer while being moved
