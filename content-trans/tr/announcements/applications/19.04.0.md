---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE Uygulamalar 19.04.'ü Gönderdi
layout: application
release: applications-19.04.0
title: KDE, KDE Uygulamalar 19.04.0'ı Gönderdi
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

KDE topluluğu, KDE Uygulamaları 19.04. piyasaya sürüldüğünü duyurmaktan mutluluk duyar

Topluluğumuz, KDE Uygulama serimizde yer alan yazılımları geliştirmek için sürekli çalışmaktadır. Yeni özelliklerin yanı sıra tüm izlencelerimizin, oyunlarımızın ve yaratıcılık araçlarımızın tasarımını, kullanılabilirliğini ve kararlılığını iyileştiriyoruz. Amacımız, KDE yazılımını daha keyifli hale getirerek hayatınızı kolaylaştırmaktır. 19.04 içinde bulacağınız tüm yeni geliştirmeleri ve hata düzeltmelerini beğeneceğinizi umuyoruz!

## What's new in KDE Applications 19.04

More than 150 bugs have been resolved. These fixes re-implement disabled features, normalize shortcuts, and solve crashes, making KDE Applications friendlier and allowing you to be more productive.

### Dosya Yönetimi

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> is KDE's file manager. It also connects to network services, such as SSH, FTP, and Samba servers, and comes with advanced tools to find and organize your data.

Yeni Özellikler:

+ We have expanded thumbnail support, so Dolphin can now display thumbnails for several new file types: <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a> files, <a href='https://phabricator.kde.org/D18738'>.epub and .fb2 eBook</a> files, <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a> files, and <a href='https://phabricator.kde.org/D19679'>PCX</a> files. Additionally, thumbnails for text files now show <a href='https://phabricator.kde.org/D19432'>syntax highlighting</a> for the text inside the thumbnail. </li>
+ You can now choose <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>which split view pane to close</a> when clicking the 'Close split' button. </li>
+ This version of Dolphin introduces <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>smarter tab placement</a>. When you open a folder in a new tab, the new tab will now be placed immediately to the right of the current one, instead of always at the end of the tab bar. </li>
+ <a href='https://phabricator.kde.org/D16872'>Tagging items</a> is now much more practical, as tags can be added or removed using the context menu. </li>
+ We have <a href='https://phabricator.kde.org/D18697'>improved the default sorting</a> parameters for some commonly-used folders. By default, the Downloads folder is now sorted by date with grouping enabled, and the Recent Documents view (accessible by navigating to recentdocuments:/) is sorted by date with a list view selected. </li>

Hata düzeltmeleri şunları içerir:

+ When using a modern version of the SMB protocol, you can now <a href='https://phabricator.kde.org/D16299'>discover Samba shares</a> for Mac and Linux machines. </li>
+ <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>Re-arranging items in the Places panel</a> once again works properly when some items are hidden. </li>
+ After opening a new tab in Dolphin, that new tab's view now automatically gains <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>keyboard focus</a>. </li>
+ Dolphin now warns you if you try to quit while the <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>terminal panel is open</a> with a program running in it. </li>
+ Dolphin'in genel performansını artırarak birçok bellek sızıntısını düzelttik.</li>

The <a href='https://cgit.kde.org/audiocd-kio.git/'>AudioCD-KIO</a> allows other KDE applications to read audio from CDs and automatically convert it into other formats.

+ The AudioCD-KIO now supports ripping into <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ We made <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>CD info text</a> really transparent for viewing. </li>

### Video Düzenleme

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

This is a landmark version for KDE's video editor. <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> has gone through an extensive re-write of its core code as more than 60%% of its internals has changed, improving its overall architecture.

İyileştirmeler şunları içerir:

+ Zaman çizelgesi, QML'den yararlanmak için yeniden yazıldı.
+ Zaman çizelgesine bir klip koyduğunuzda, ses ve video her zaman ayrı parçalara gider.
+ The timeline now supports keyboard navigation: clips, compositions and keyframes can be moved with the keyboard. Also, the height of the tracks themselves is adjustable.
+ In this version of Kdenlive, the in-track audio recording comes with a new <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>voice-over</a> feature.
+ We have improved copy/paste: it works between different project windows. The proxy clip management has also been enhanced, as clips can now be individually deleted.
+ Version 19.04 sees the return of support for external BlackMagic monitor displays and there are also new on-monitor preset guides.
+ We have improved the keyframe handling, giving it a more consistent look and workflow. The titler has also been improved by making the align buttons snap to safe zones, adding configurable guides and background colors, and displaying missing items.
+ Bir grup klibi taşıdığınızda tetiklenen klipleri yanlış yerleştiren veya kaçıran zaman çizelgesi bozulması hatasını düzelttik.
+ Windows'ta resimleri beyaz ekran olarak oluşturan JPG resim hatasını düzelttik. Windows'ta ekran yakalamayı etkileyen hataları da düzelttik.
+ Yukarıdakilerin hepsinin yanı sıra, Kdenlive kullanımını daha kolay ve sorunsuz hale getirecek birçok küçük kullanılabilirlik geliştirmesi ekledik.

### Ofis

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/okular/'>Okular</a>, KDE'nin çok amaçlı belge görüntüleyicisidir. PDF'leri okumak ve bunlara açıklama eklemek için idealdir; ODF dosyalarını (LibreOffice ve OpenOffice tarafından kullanıldığı gibi), ePub dosyaları olarak yayınlanan e-kitapları, en yaygın çizgi roman dosyalarını, PostScript dosyalarını ve daha fazlasını açabilir.

İyileştirmeler şunları içerir:

+ To help you ensure your documents are always a perfect fit, we've added <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>scaling options</a> to Okular's Print dialog.
+ Okular artık PDF dosyalarında <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>dijital imzaları</a> görüntülemeyi ve doğrulamayı destekliyor.
+ Thanks to improved cross-application integration, Okular now supports editing LaTeX documents in <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ <a href='https://phabricator.kde.org/D18118'>Dokunmatik ekranda gezinme</a> için iyileştirilmiş destek, Sunum modundayken bir dokunmatik ekran kullanarak geri ve ileri hareket edebileceğiniz anlamına gelir.
+ Users who prefer manipulating documents from the command-line will be able to perform smart <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>text search</a> with the new command-line flag that lets you open a document and highlight all occurrences of a given piece of text.
+ Okular now properly displays links in <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>Markdown documents</a> that span more than one line.
+ The <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>trim tools</a> have fancy new icons.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

<a href='https://kde.org/applications/internet/kmail/'>KMail</a> is KDE's privacy-protecting email client. Part of the <a href='https://kde.org/applications/office/kontact/'>Kontact groupware suite</a>, KMail supports all email systems and allows you to organize your messages into a shared virtual inbox or into separate accounts (your choice). It supports all kinds of message encryption and signing, and lets you share data such as contacts, meeting dates, and travel information with other Kontact applications.

İyileştirmeler şunları içerir:

+ Move over, Grammarly! This version of KMail comes with support for languagetools (grammar checker) and grammalecte (French-only grammar checker).
+ E-postalardaki telefon numaraları artık algılanıyor ve doğrudan <a href='https://community.kde.org/KDEConnect'>KDE Bağlan</a> üzerinden aranabiliyor.
+ KMail now has an option to <a href='https://phabricator.kde.org/D19189'>start directly in system tray</a> without opening the main window.
+ We have improved the Markdown plugin support.
+ Oturum açma başarısız olduğunda IMAP üzerinden posta alma artık takılı kalmıyor.
+ Güvenilirliği ve performansı iyileştirmek için K Posta'nın arka ucu Akonadi'de de çok sayıda düzeltme yaptık.

<a href='https://kde.org/applications/office/korganizer/'>K Organizatör</a>, tüm etkinliklerinizi yöneten Kontak'ın takvim bileşenidir.

+ Recurrent events from <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a> are again synchronized correctly.
+ The event reminder window now remembers to <a href='https://phabricator.kde.org/D16247'>show on all desktops</a>.
+ We modernized the look of the <a href='https://phabricator.kde.org/T9420'>event views</a>.

<a href='https://cgit.kde.org/kitinerary.git/'>KDE Yol Kılavuzu</a>, Kontak'ın size yolculuğunuzda yolunuzu bulmanıza yardımcı olacak yepyeni yolculuk asistanıdır.

- There is a new generic extractor for RCT2 tickets (e.g. used by railway companies such as DSB, ÖBB, SBB, NS).
- Havaalanı adı tespiti ve belirsizliği giderme büyük ölçüde geliştirildi.
- We added new custom extractors for previously unsupported providers (e.g. BCD Travel, NH Group), and improved format/language variations of already supported providers (e.g. SNCF, Easyjet, Booking.com, Hertz).

### Geliştirme

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

<a href='https://kde.org/applications/utilities/kate/'>Kate</a> is KDE's full-featured text editor, ideal for programming thanks to features such as tabs, split-view mode, syntax highlighting, a built-in terminal panel, word completion, regular expressions search and substitution, and many more via the flexible plugin infrastructure.

İyileştirmeler şunları içerir:

- Kate artık yalnızca bazılarını değil, tüm görünmez boşluk karakterlerini gösterebilir.
- Genel varsayılan ayarı değiştirmek zorunda kalmadan her belge için kendi menü girişini kullanarak Statik Sözcük Kaydırmayı kolayca etkinleştirebilir ve devre dışı bırakabilirsiniz.
- The file and tab context menus now include a bunch of useful new actions, such as Rename, Delete, Open Containing Folder, Copy File Path, Compare [with another open file], and Properties.
- Kate'in bu sürümü, popüler ve kullanışlı satır içi Terminal özelliği de dahil olmak üzere varsayılan olarak etkinleştirilen daha fazla eklentiyle birlikte gelir.
- When quitting, Kate no longer prompts you to acknowledge files that were modified on the disk by some other process, such as a source control change.
- The plugin’s tree view now correctly displays all menu items for git entries that have umlauts in their names.
- When opening multiple files using the command line, the files are opened in new tabs in the same order as specified in the command line.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

<a href='https://kde.org/applications/system/konsole/'>Konsole</a> is KDE's terminal emulator. It supports tabs, translucent backgrounds, split-view mode, customizable color schemes and keyboard shortcuts, directory and SSH bookmarking, and many other features.

İyileştirmeler şunları içerir:

+ Tab management has seen a number of improvements that will help you be more productive. New tabs can be created by middle-clicking on empty parts of the tab bar, and there's also an option that allows you to close tabs by middle-clicking on them. Close buttons are displayed on tabs by default, and icons will be displayed only when using a profile with a custom icon. Last but not least, the Ctrl+Tab shortcut allows you to quickly switch between the current and previous tab.
+ The Edit Profile dialog received a huge <a href='https://phabricator.kde.org/D17244'>user interface overhaul</a>.
+ The Breeze color scheme is now used as the default Konsole color scheme, and we have improved its contrast and consistency with the system-wide Breeze theme.
+ Kalın yazı görüntülenirken karşılaşılan sorunları çözdük.
+ Konsole artık altı çizili stil imleci doğru şekilde görüntülüyor.
+ We have improved the display of <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>box and line characters</a>, as well as of <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>Emoji characters</a>.
+ Profile switching shortcuts now switch the current tab’s profile instead of opening a new tab with the other profile.
+ Inactive tabs that receive a notification and have their title text color changed now again reset the color to the normal tab title text color when the tab is activated.
+ The 'Vary the background for each tab' feature now works when the base background color is very dark or black.

<a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> is a computer-aided translation system that focuses on productivity and quality assurance. It is targeted at software translation, but also integrates external conversion tools for translating office documents.

İyileştirmeler şunları içerir:

- Lokalize artık çeviri kaynağını özel bir düzenleyiciyle görüntülemeyi destekliyor.
- We improved the DockWidgets location, and the way settings are saved and restored.
- .po dosyalarındaki konum artık mesajlar filtrelenirken korunmaktadır.
- We fixed a number of UI bugs (developer comments, RegExp toggle in mass replace, fuzzy empty message count, …).

### İzlenceler

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> is an advanced image viewer and organizer with intuitive and easy-to-use editing tools.

İyileştirmeler şunları içerir:

+ 19.04 uygulamalarıyla birlikte gelen Gwenview sürümü, kaydırma, yakınlaştırma, kaydırma ve daha fazlası için hareketlerle tam <a href='https://phabricator.kde.org/D13901'>dokunmatik ekran desteği</a> içerir.
+ Another enhancement added to Gwenview is full <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>High DPI support</a>, which will make images look great on high-resolution screens.
+ Improved support for <a href='https://phabricator.kde.org/D14583'>back and forward mouse buttons</a> allows you to navigate between images by pressing those buttons.
+ You can now use Gwenview to open image files created with <a href='https://krita.org/'>Krita</a> – everyone’s favorite digital painting tool.
+ Gwenview now supports large <a href='https://phabricator.kde.org/D6083'>512 px thumbnails</a>, allowing you to preview your images more easily.
+ Gwenview now uses the <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>standard Ctrl+L keyboard shortcut</a> to move focus to the URL field.
+ You can now use the <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>Filter-by-name feature</a> with the Ctrl+I shortcut, just like in Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> is Plasma's screenshot application. You can grab full desktops spanning several screens, individual screens, windows, sections of windows, or custom regions using the rectangle selection feature.

İyileştirmeler şunları içerir:

+ We have extended the Rectangular Region mode with a few new options. It can be configured to <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>auto-accept</a> the dragged box instead of asking you to adjust it first. There is also a new default option to remember the current <a href='https://phabricator.kde.org/D19117'>rectangular region</a> selection box, but only until the program is closed.
+ You can configure what happens when the screenshot shortcut is pressed <a href='https://phabricator.kde.org/T9855'>while Spectacle is already running</a>.
+ Spectacle allows you to change the <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>compression level</a> for lossy image formats.
+ Save settings shows you what the <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>filename of a screenshot</a> will look like. You can also easily tweak the <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>filename template</a> to your preferences by simply clicking on placeholders.
+ Spectacle no longer displays both “Full Screen (All Monitors)” and “Current Screen” options when your computer only has one screen.
+ The help text in Rectangular Region mode now shows up in the middle of the primary display, rather than split between the screens.
+ Wayland'de çalıştırıldığında, Spectacle yalnızca çalışan özellikleri içerir.

### Oyunlar ve Eğitim

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

Our application series includes numerous <a href='https://games.kde.org/'>games</a> and <a href='https://edu.kde.org/'>educational applications</a>.

<a href='https://kde.org/applications/education/kmplot'>KmPlot</a> is a mathematical function plotter. It has a powerful built-in parser. The graphs can be colorized and the view is scalable, allowing you to zoom to the level you need. Users can plot different functions simultaneously and combine them to build new functions.

+ You can now zoom in by holding down Ctrl and using the <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>mouse wheel</a>.
+ This version of Kmplot introduces the <a href='https://phabricator.kde.org/D17626'>print preview</a> option.
+ Root value or (x,y) pair can now be copied to <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>clipboard</a>.

<a href='https://kde.org/applications/games/kolf/'>Kolf</a> is a miniature golf game.

+ We have restored <a href='https://phabricator.kde.org/D16978'>sound support</a>.
+ Kolf has been successfully ported away from kdelibs4.
