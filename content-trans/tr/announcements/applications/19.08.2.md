---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE Uygulamalar 19.08.2'yi Gönderdi.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE Uygulamalar 19.08.2'yi Gönderdi
version: 19.08.2
---
{{% i18n_date %}}

Bugün KDE, <a href='../19.08.0'>KDE Uygulamaları 19.08</a> için ikinci kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize, Spectacle, among others.

İyileştirmeler şunları içerir:

- Konsole ve diğer uygulamalarda yüksek DPI desteği geliştirildi
- Dolphin'de farklı aramalar arasında geçiş yapmak artık arama parametrelerini doğru şekilde güncelliyor
- K Posta, iletileri doğrudan uzak klasörlere yeniden kaydedebilir
