---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE Uygulamalar 19.04.3.'ü Gönderdi
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE, KDE Uygulamaları 19.04.3'ü Gönderdi
version: 19.04.3
---
{{% i18n_date %}}

Bugün KDE  <a href='../19.04.0'>KDE Uygulamaları 19.04</a>için üçüncü kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

Over sixty recorded bugfixes include improvements to Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular, Umbrello, among others.

İyileştirmeler şunları içerir:

- Konqueror and Kontact no longer crash on exit with QtWebEngine 5.13
- Cutting groups with compositions no longer crashes the Kdenlive video editor
- The Python importer in Umbrello UML designer now handles parameters with default arguments
