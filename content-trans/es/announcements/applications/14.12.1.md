---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE lanza las Aplicaciones de KDE 14.12.1.
layout: application
title: KDE lanza las Aplicaciones de KDE 14.12.1
version: 14.12.1
---
Hoy, 13 de enero de 2015. KDE ha lanzado la primera actualización de estabilización para las <a href='../14.12.0'>Aplicaciones</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 50 soluciones de errores registrados, se incluyen mejoras para la herramienta de compresión Ark, para la herramienta de modelado UML Umbrello, para el visor de documentos Okular, para la aplicación de aprendizaje de pronunciación Artikulate y para el cliente de escritorio remoto KRDC.

También se incluyen versiones de los Espacios de trabajo Plasma 4.11.15, de la Plataforma de desarrollo de KDE 4.14.4 y de la suite Kontact 4.14.4 que contarán con asistencia a largo plazo.
