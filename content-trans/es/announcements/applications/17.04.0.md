---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: KDE lanza las Aplicaciones de KDE 17.04.0
layout: application
title: KDE lanza las Aplicaciones de KDE 17.04.0
version: 17.04.0
---
20 de abril de 2017. Las Aplicaciones de KDE 17.04 están aquí. En general, hemos trabajado para hacer que tanto las aplicaciones como las bibliotecas en las que se apoyan sean más estables y más fáciles de usar. Planchando las arrugas y oyendo los comentarios recibidos, hemos hecho que las Aplicaciones de KDE sean menos propensas a sufrir fallos técnicos y mucho más amigables de usar.

¡Disfrute de las nuevas aplicaciones!

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

Los desarrolladores de KAlgebra siguen su particular camino hacia la convergencia, habiendo portado la versión móvil del completo programa educativo a Kirigami 2.0 (la infraestructura preferida para integrar aplicaciones de KDE en el escritorio y en plataformas móviles).

Además, la versión para el escritorio también ha migrado el motor 3D a GLES, el software que permite al programa generar funciones en 3D tanto en el escritorio como en dispositivos móviles. Esto simplifica el código fuente y lo hace más fácil de mantener.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

El editor de vídeo de KDE se está haciendo más estable y más rico en funciones con cada nueva versión. En esta ocasión, los desarrolladores han rediseñado el diálogo de selección de perfiles para facilitar la definición del tamaño de la pantalla, la velocidad de los fotogramas y otros parámetros de la película.

Ahora también es posible reproducir el vídeo directamente desde la notificación cuando se ha terminado de renderizar. Se han corregido algunos cuelgues de la aplicación que se producían al mover clips en la línea temporal. También se ha mejorado el asistente de DVD.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

Nuestro navegador de archivos favorito y portal para todo (excepto, tal vez, para el inframundo) ha recibido algunos cambios de imagen y mejoras de usabilidad para hacerlo todavía más potente.

Los menús de contexto del panel <i>Lugares</i> (a la izquierda del área de visualización, de forma predeterminada) se han limpiado, y ahora es posible interactuar con los campos que contienen metadatos en las ayudas emergentes. Por cierto, estas ayudas emergentes también funcionan ahora en Wayland.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

La popular aplicación gráfica para crear, descomprimir y gestionar archivos comprimidos proporciona ahora la función <i>Buscar</i> para ayudarle a encontrar archivos dentro de archivos comprimidos muy atestados.

También le permite activar y desactivar complementos directamente desde el diálogo <i>Configurar</i>. Y hablando de complementos, el nuevo complemento «Libzip» mejora el trabajo con archivos zip.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Si está enseñando o aprendiendo música, tiene que probar Minuet. La nueva versión ofrece más ejercicios de escalas y tareas para desarrollar el oído para las escalas bebop, menor/mayor armónica, pentatónica y simétrica.

También es posible definir y realizar pruebas usando el nuevo <i>Modo de pruebas</i> para responder ejercicios. Puede controlar su progreso ejecutando una secuencia de 10 ejercicios para obtener una estadística de aciertos al terminar.

### ¡Y aún más!

<a href='https://okular.kde.org/'>Okular</a>, el visor de documentos de KDE, contiene al menos media docena de cambios que añaden funciones y redoblan su usabilidad en pantallas táctiles. <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> y varias aplicaciones más que forman <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (la suite de ofimática de KDE con correo, calendario y trabajo en grupo) se han revisado, depurado y optimizado para ayudarle a ser más productivo.

<a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a> y <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a>, entre otras (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>notas del lanzamiento</a>) se han migrado a KDE Frameworks 5. Esperamos con ilusión sus comentarios sobre las funcionalidades más recientes introducidas en esta versión.

<a href='https://userbase.kde.org/K3b'>K3b</a> se ha unido al lanzamiento de las Aplicaciones de KDE.

### A la caza de errores

Se han resuelto más de 95 errores en aplicaciones, que incluyen Kopete, KWalletManager, Marble y Spectacle, entre otras.

### Registro de cambios completo
