---
aliases:
- ../../kde-frameworks-5.39.0
date: 2017-10-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Coincidir únicamente tipos MIME reales, no los semejantes a «imagen de CD en bruto» (fallo 364884).
- Remove pf.path() from container before the reference got screwed up by it.remove()
- Fix tags KIO-slave protocol description
- Considerar los archivos Markdown como documentos.

### Iconos Brisa

- Se ha añadido el icono «overflow-menu» (error 385171).

### Módulos CMake adicionales

- Corregir la compilación con dependencias de python tras 7af93dd23873d0b9cdbac192949e7e5114940aa6

### Integración con Frameworks

- Hacer que «KStandardGuiItem::discard» coincida con «QDialogButtonBox::Discard».

### KActivitiesStats

- Se ha cambiado el límite de la consulta predeterminada a cero.
- Se ha añadido la opción para activar el probador del modelo.

### KCMUtils

- Hacer que «KCMultiDialog» se pueda desplazar (error 354227).

### KConfig

- Marcar como obsoleto «KStandardShortcut::SaveOptions».

### KConfigWidgets

- Marcar como obsoletos «KStandardAction::PasteText» y «KPasteTextAction».

### KCoreAddons

- desktoptojson: Se ha mejorado la heurística de detección de tipos de servicios heredados (fallo 384037).

### KDeclarative

- Se ha cambiado la licencia a LGPL2.1+.
- Se ha añadido el método «openService()» a «KRunProxy».

### KFileMetaData

- Se ha corregido un cuelgue cuando se destruyen más de una instancia de «ExtractorCollection».

### KGlobalAccel

- Revert "KGlobalAccel: port to KKeyServer's new method symXModXToKeyQt, to fix numpad keys" (bug 384597)

### KIconThemes

- Se ha añadido un método para reiniciar la paleta personalizada.
- Usar «qApp-&gt;palette()» cuando no se ha asignado una personalizada.
- Asignar el tamaño de búfer correcto.
- Permitir la definición de una paleta personalizada en lugar de «colorSets».
- Exponer el conjunto de colores para la hoja de estilo.

### KInit

- Windows: Fix 'klauncher uses absolute compile time install path for finding kioslave.exe'

### KIO

- kioexec: Watch the file when it has finished copying (bug 384500)
- KFileItemDelegate: Reservar siempre espacio para los iconos (error 372207).

### Kirigami

- don't instantiate Theme file in BasicTheme
- Añadir un nuevo botón de avance.
- less contrast to the sheet scrollbar background
- more reliable insert and remove from overflow menu
- better context icon rendering
- Tener más cuidado para centrar el botón de acción.
- use iconsizes for action buttons
- pixel perfect icon sizes on desktop
- selected effect to fake handle icon
- Se ha corregido el color de las asas.
- Mejor color para el botón de la acción principal.
- fix context menu for desktop style
- better "more" menu for the toolbar
- a proper menu for the intermediate pages context menu
- add a text field which should bring up a keypad
- No producir un cuelgue cuando se lanza con estilos no existentes.
- Concepto de «ColorSet» en los temas.
- Simplificar la gestión de la rueda (error 384704).
- Nueva aplicación de ejemplo con archivos «main» en qml para escritorio/móvil.
- Asegurarse de que «currentIndex» es válido.
- Generar los metadatos de «appstream» de la aplicación de la galería.
- Look for QtGraphicalEffects, so packagers don't forget it
- No incluir el control sobre la decoración inferior (error 384913).
- Coloreado más claro cuando la «listview» no tiene «activeFocus».
- Permitir el uso parcial de disposiciones RTL.
- Desactivar los accesos rápidos cuando se desactiva una acción.
- Crear la estructura completa del complemento en el directorio «build».
- Corregir la accesibilidad de la página principal de la galería.
- If plasma isn't available, KF5Plasma isn't either. Should fix the CI error

### KNewStuff

- Requerir Kirigami 2.1 en lugar de 1.0 para «KNewStuffQuick».
- Crear correctamente «KPixmapSequence».
- Don't complain the knsregistry file is not present before it's useful

### Framework KPackage

- kpackage: Empaquetar una copia de «servicetypes/kpackage-generic.desktop».
- kpackagetool: Empaquetar una copia de «servicetypes/kpackage-generic.desktop».

### KParts

- KPartsApp template: fix install location of kpart desktop file

### KTextEditor

- Ignore default mark in icon border for single selectable mark
- Usar «QActionGroup» para la selección del modo de entrada.
- Corregir la barra de comprobación ortográfica que faltaba (error 359682).
- Fix the fall-back "blackness" value for unicode &gt; 255 characters (bug 385336)
- Fix trailing space visualization for RTL lines

### KWayland

- Only send OutputConfig sendApplied / sendFailed to the right resource
- Don't crash if a client (legally) uses deleted global contrast manager
- Permitir el uso de XDG v6.

### KWidgetsAddons

- KAcceleratorManager: set icon text on actions to remove CJK markers (bug 377859)
- KSqueezedTextLabel: Squeeze text when changing indent or margin
- Use edit-delete icon for destructive discard action (bug 385158)
- Fix Bug 306944 - Using the mousewheel to increment/decrement the dates (bug 306944)
- KMessageBox: Usar el icono del signo de interrogación en los diálogos de preguntas.
- KSqueezedTextLabel: Respect indent, margin and frame width

### KXMLGUI

- Fix KToolBar repaint loop (bug 377859)

### Framework de Plasma

- Corregir «org.kde.plasma.calendar» con Qt 5.10.
- [FrameSvgItem] Iterate child nodes properly
- [Interfaz de contenedores] No añadir acciones de contenedores a las acciones de la miniaplicación en el escritorio.
- Add new component for the greyed out labels in Item Delegates
- Fix FrameSVGItem with the software renderer
- Don't animate IconItem in software mode
- [FrameSvg] Usar el nuevo estilo de «connect».
- possibility to set an attached colorscope to not inherit
- Add extra visual indicator for Checkbox/Radio keyboard focus
- No volver a crear un «pixmap» nulo.
- Pass item to rootObject() since it's now a singleton (bug 384776)
- No listar los nombres de las pestañas dos veces.
- No aceptar el foco activo en la pestaña.
- Registrar la revisión 1 para «QQuickItem».
- [Componentes de Plasma 3] Se ha corregido RTL en algunos widgets.
- Se ha corregido el identificador no válido en el elemento de la vista.
- update mail notification icon for better contrast (bug 365297)

### qqc2-desktop-style

New module: QtQuickControls 2 style that uses QWidget's QStyle for painting This makes it possible to achieve an higher degree of consistency between QWidget-based and QML-based apps.

### Solid

- [solid/fstab] Add support for x-gvfs style options in fstab
- [solid/fstab] Swap vendor and product properties, allow i18n of description

### Resaltado de sintaxis

- Se han corregido las referencias no válidas de «itemData» en 57 archivos de resaltado de sintaxis.
- Add support for custom search paths for application-specific syntax and theme definitions
- AppArmor: Corregir reglas de DBus.
- Highlighting indexer: factor out checks for smaller while loop
- ContextChecker: support '!' context switching and fallthroughContext
- Highlighting indexer: check existence of referenced context names
- Se ha cambiado la licencia del resaltado sintáctico de qmake a la licencia de MIT.
- Let qmake highlighting win over Prolog for .pro files (bug 383349)
- Support clojure's "@" macro with brackets
- Se ha añadido resaltado de sintaxis para perfiles de AppArmor.
- Highlighting indexer: Catch invalid a-Z/A-z ranges in regexps
- Fixing incorrectly capitalized ranges in regexps
- add missing reference files for tests, looks ok, I think
- Se ha añadido el uso de archivos HEX de Intel en la base de datos de resaltado de sintaxis.
- Disable spell checking for strings in Sieve scripts

### ThreadWeaver

- Se ha corregido una fuga de memoria.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
