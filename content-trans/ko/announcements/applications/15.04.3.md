---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE에서 KDE 프로그램 15.04.3 출시
layout: application
title: KDE에서 KDE 프로그램 15.04.3 출시
version: 15.04.3
---
July 1, 2015. Today KDE released the third stability update for <a href='../15.04.0'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kdenlive, kdepim, Kopete, ktp-contact-list, Marble, Okteta 및 Umbrello 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.21, KDE Development Platform 4.14.10 and the Kontact Suite 4.14.10.
