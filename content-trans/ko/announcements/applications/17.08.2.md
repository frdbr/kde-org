---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE에서 KDE 프로그램 17.08.2 출시
layout: application
title: KDE에서 KDE 프로그램 17.08.2 출시
version: 17.08.2
---
October 12, 2017. Today KDE released the second stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Dolphin, Gwenview, Kdenlive, Marble, Okular 등에 25개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.37.

개선 사항:

- Plasma 행사 플러그인 설정의 메모리 누수와 충돌 개선
- 더 이상 Akregator의 읽지 않은 글 필터에서 읽은 메시지를 즉시 제거하지 않음
- Gwenview 가져오기 도구에서 EXIF 날짜와 시간 사용
