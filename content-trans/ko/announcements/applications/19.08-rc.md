---
aliases:
- ../announce-applications-19.08-rc
date: 2019-08-02
description: KDE Ships Applications 19.08 Release Candidate.
layout: application
release: applications-19.07.90
title: KDE에서 KDE 프로그램 19.08의 출시 후보 출시
version_number: 19.07.90
version_text: 19.08 Release Candidate
---
August 02, 2019. Today KDE released the Release Candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/19.08_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

KDE 프로그램 19.08 릴리스의 품질 및 사용자 경험 유지, 향상을 위하여 지속적인 테스트가 필요합니다. 개발자들이 모든 환경을 테스트하기는 어렵기 때문에 KDE의 높은 품질을 유지하려면 사용자 여러분들의 도움이 필요합니다. 최종 릴리스 이전에 버그를 빠르게 찾으려면 여러분의 도움이 필요합니다. 릴리스 후보 버전을 설치하고 <a href='https://bugs.kde.org/'>버그 보고(영어)</a>를 통해서 참여할 수 있습니다.
