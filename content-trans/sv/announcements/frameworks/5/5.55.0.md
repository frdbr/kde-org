---
aliases:
- ../../kde-frameworks-5.55.0
date: 2019-02-09
layout: framework
libCount: 70
---
### Baloo

- [tags_kio] Inaktivera åtkomst för webbadress med dubbla snedstreck, dvs. "tags://" (fel 400594)
- Instantiera QApplication före KCrash/KCatalog
- Ignorera alla deviceAdded signaler från Solid som inte gäller lagring
- Använd den trevligare K_PLUGIN_CLASS_WITH_JSON
- Ta bort check av Qt 5.10 nu när den krävs som minimal version

### Breeze-ikoner

- Lägg till inställningsmodulikon för markörer
- Lägg till och byt namn på vissa YaST-ikoner och symboliska länkar
- Förbättra klockikonen för underrättelser genom att använda designen från Kalarm (fel 400570)
- Lägg till yast-upgrade
- Förbättra ikonerna weather-storm-* (fel 403830)
- Lägg till font-otf symboliska länkar, precis som font-ttf symboliska länkar
- Lägg till riktiga edit-delete-shred ikoner (fel 109241)
- Lägg till ikoner för beskärningsmarginaler och beskär till markering (fel 401489)
- Ta bort symboliska länkar för edit-delete-shred som förberedelse för att ersätta dem med riktiga ikoner
- Byt namn på inställningsmodulikon Aktiviteter
- Lägg till inställningsmodulikon för Aktiviteter
- Lägg till inställningsmodulikon för Virtuella skrivbord
- Lägg till inställningsmodulikoner för pekskärm och skärmkant
- Rätta ikonnamn för inställningar relaterade till fildelning
- Lägg till ikonen preferences-desktop-effects
- Lägg till en Plasmatema inställningsikon
- Förbättra kontrast för preferences-system-time (fel 390800)
- Lägg till en ny preferences-desktop-theme-global ikon
- Lägg till verktygsikon
- Ikonen document-new följer färgschema-text
- Lägg till preferences-system-splash ikon för inställningsmodulen Startskärm
- Inkludera miniprogram/22
- Lägg till Kotlin (.kt) Mime-typ ikoner
- Förbättra ikonen preferences-desktop-cryptography
- Fyll i lås i preferences-desktop-user-password
- Fyll i låset i krypteringsikonen konsekvent
- Använd en Kile-ikon som liknar originalet

### Extra CMake-moduler

- FindGperf: Ställ in SKIP_AUTOMOC för genererad fil för ecm_gperf_generate
- Flytta -Wsuggest-override -Wlogical-op till vanliga kompilatorinställningar
- Rätta generering av Python-bindningar för klasser med borttagna kopieringskonstruktorer
- Rätta generering av gmake-moduler för Qt 5.12.1
- Använd mer https i länkar
- Dokumentation av programmeringsgränssnitt: Lägg till saknade poster för vissa find-modules &amp; modules
- FindGperf: Förbättra dokumentation av programmeringsgränssnitt: exempel på användning av uppmärkning
- ECMGenerateQmlTypes: Förbättra dokumentation av programmeringsgränssnitt:  Titeln behöver fler --- markup
- ECMQMLModules: Förbättra dokumentation av programmeringsgränssnitt:  Lägg till saknad "Since"
- FindInotify: Rätta dokumentation av programmeringsgränssnitt .rst tagg, lägg till saknad "Since"

### KActivities

- Rättning för MacOS

### KArchive

- Spara två KFilterDev::compressionTypeForMimeType anrop

### KAuth

- Ta bort stöd för att skicka QVariants för grafiskt användargränssnitt till KAuth hjälpfunktioner

### KBookmarks

- Bygg utan D-Bus på Android
- Lägg till const

### KCMUtils

- [kcmutils] Lägg till utelämningstecken i sökbeteckningar i KPluginSelector

### KCodecs

- nsSJISProber::HandleData: Krascha inte om aLen är 0

### KConfig

- kconfig_compiler: Ta bort tilldelningsoperator och kopieringskonstruktor

### KConfigWidgets

- Bygg utan KAuth och D-Bus på Android
- Lägg till KLanguageName

### KCrash

- Kommentera varför ändring av ptracer krävs
- [KCrash] Etablera uttag för att tillåta ändring av ptracer

### KDeclarative

- [KCM Controls GridView] Lägg till ta bort animering

### Stöd för KDELibs 4

- Rätta vissa landsflaggor för att använda alla bildpunktsavbildningar

### KDESU

- Hantera felaktigt lösenord när sudo används som frågar efter ett annan lösenord (fel 389049)

### KFileMetaData

- exiv2extractor: Lägg till stöd för bmp, gif, webp, tga
- Rätta misslyckad test av exiv GPS-data
- Testa tom och noll GPS-data
- Lägg till stöd för fler Mime-typer i taglibwriter

### KHolidays

- holidays/plan2/holiday_ua_uk - Uppdaterad för 2019

### KHTML

- Lägg till JSON metadata to khtmlpart plugin binary

### KIconThemes

- Bygg utan D-Bus på Android

### KImageFormats

- xcf: Rätta fix för ogenomskinlighet utanför gränser
- Ta bort kommentarer för inkludering av qdebug
- tga: Rätta Use-of-uninitialized-value för felaktiga filer
- Maximal ogenomskinlighet är 255
- xcf: Rätta assert i filer med två PROP_COLORMAP
- ras: Rätta assert på grund av att ColorMapLength är för stor
- pcx: Rätta krasch för inexakt fil
- xcf: Implementera robusthet för PROP_APPLY_MASK inte finns i filen
- xcf: loadHierarchy: Följ lagertypen och inte BPP
- tga: Stöd inte mer än 8 alfa-bitar
- ras: Returnera false om allokering av bilden misslyckas
- rgb: Rätta heltalsöverflöde i inexakt fil
- rgb: Rätta heap bufferöverflöde i inexakt fil
- psd: Rätta krasch för inexakt fil
- xcf: Initiera x/y_offset
- rgb: Rätta krasch för inexakt bild
- pcx: Rätta krasch för inexakt bild
- rgb: Rätta krasch för inexakt fil
- xcf: Initiera lagerläge
- xcf: Initiera lagerogenomskinlighet
- xcf: Ställ in buffer till 0 vid läsning av mindre data än förväntat
- bzero -&gt; memset
- Rätta diverse OOB-läsningar och skrivningar i kimg_tga och kimg_xcf
- pic: Ändra tillbaka huvud-id om 4 byte inte lästes som förväntat
- xcf: Ställ in buffer till 0 vid läsning av mindre data än förväntat
- xcf: Anropa bara setDotsPerMeterX/Y om PROP_RESOLUTION hittas
- xcf: Initiera num_colors
- xcf: Initiera egenskap för lagersynlighet
- xcf: Typkonvertera inte int till uppräkningstyp som inte kan innehålla int-värdet
- xcf: Låt inte int överflöda vi anropen setDotsPerMeterX/Y

### KInit

- KLauncher: Hantera processer som avslutas utan fel (fel 389678)

### KIO

- Förbättra tangentbordskontroll av grafisk checksummeringskomponent
- Lägg till hjälpfunktioner för att inaktivera omdirigering (användbar för kde-open)
- Återställ "Omstrukturera SlaveInterface::calcSpeed" (fel 402665)
- Ställ inte in CMake policy CMP0028 till gammal. Vi har inte mål med :: om de inte importeras.
- [kio] Lägg till utelämningstecken i sökbeteckningen för kaksektionen
- [KNewFileMenu] Skicka inte fileCreated när en katalog skapas (fel 403100)
- Använd (och föreslå användning av) den snyggare K_PLUGIN_CLASS_WITH_JSON
- Undvik att kio_http_cache_cleaner blockeras och säkerställ avslutning med session (fel 367575)
- Rätta felande test av knewfilemenu och felets underliggande orsak

### Kirigami

- Höj bara färgade knappar (fel 403807)
- Rätta höjd
- Samma policy för storleksändring av marginaler som andra listposter
- === operatorer
- Stöd för konceptet med expanderbara objekt
- Rensa inte när alla sidorna ersätts
- Horisontell vaddering är dubbel mot vertikal
- Ta hänsyn till vaddering för att beräkna storlekstips
- [kirigami] Använd inte lätta teckensnittsstilar för rubriker (2/3) (fel 402730)
- Laga Om-sidans layout på mindre enheter
- stopatBounds för bläddringsbar länkstig

### KItemViews

- [kitemviews] Ändra sökningen i skrivbordets beteende/aktiviteter för att vara mer liknande andra sökbeteckningar

### KJS

- Ställ in SKIP_AUTOMOC för vissa genererade filer, för att hantera CMP0071

### KNewStuff

- Rätta semantik för ghns_exclude (fel 402888)

### KNotification

- Rätta minnesläcka när ikondata skickas till Java
- Ta bort beroende av AndroidX stödbibliotek
- Lägg till stöd för Android underrättelsekanal
- Få underrättelser att fungera på Android med programmeringsgränssnitt nivå &lt; 23
- Flytta kontroll av Android programmeringsgränssnitt till körningstid
- Ta bort oanvänd framåtdeklaration
- Bygg om AAR när Java-källkod ändras
- Bygg Java-sidan med Gradle, som AAR istället för JAR
- Lita inte på integrering av Plasma arbetsyta på Android
- Sök efter inställning av underrättelsehändelse i qrc-resurser

### Ramverket KPackage

- Få översättningar att fungera

### KPty

- Rätta missanpassning mellan struct och klass

### Kör program

- Ta bort explicit användning av ECM_KDE_MODULE_DIR, en del av ECM_MODULE_PATH

### KService

- Bygg utan D-Bus på Android
- Föreslå att man använder K_PLUGIN_CLASS_WITH_JSON

### KTextEditor

- Qt 5.12.0 har problem med implementering av reguljära uttryck i QJSEngine, indentering kan bete sig felaktigt, Qt 5.12.1 kommer att ha en korrigering
- KateSpellCheckDialog: Ta bort åtgärden "Val av stavningskontroll"
- Uppdatera JavaScript-biblioteket underscore.js till version 1.9.1
- Rätta fel 403422: Tillåt ändring av markörstorleken igen (fel 403422)
- Sökrad: Lägg till knappen Avbryt för att stoppa aktiviteter som kör länge (fel 244424)
- Ta bort explicit användning av ECM_KDE_MODULE_DIR, en del av ECM_MODULE_PATH
- Granska KateGotoBar
- ViewInternal: Rätta 'Gå till matchande parentes' i överskridningsläge (fel 402594)
- Använd HTTPS, om tillgängligt, i länkar synliga för användare
- Granska KateStatusBar
- ViewConfig: Lägg till alternativ för att klistra in på markörpositionen med musen (fel 363492)
- Använd den trevligare K_PLUGIN_CLASS_WITH_JSON

### Kwayland

- [server] Skapa riktig touch ids
- Gör så att XdgTest följer specifikation
- Lägg till alternativ för att använda wl_display_add_socket_auto
- [server] Skicka ursprungliga org_kde_plasma_virtual_desktop_management.rows
- Lägg till radinformation i Plasma virtuella skrivbordsprotokollet
- [klient] Svep in wl_shell_surface_set_{class,title}
- Skydda resursborttagning i OuptutConfiguration::sendApplied

### KWidgetsAddons

- [KWidgetsAddons] Använd inte lätta teckensnittsstilar för rubriker (3/3) (fel 402730)

### KXMLGUI

- Bygg utan D-Bus på Android
- Gör KCheckAccelerators mindre påträngande för program som inte länkar direkt till KXmlGui

### ModemManagerQt

- Rätta implementeringen av operator &gt;&gt; i QVariantMapList

### Plasma ramverk

- [mallar för skrivbordsunderlägg] Lägg till saknad Comment= post i skrivbordsfil
- Dela instanser av Plasma::Theme mellan olika ColorScope
- Gör klockans SVG-skuggor mer logiskt riktiga och visuellt lämpliga (fel 396612)
- [ramverk] Använd inte lätta teckensnittsstilar för rubriker (1/3) (fel 402730)
- [Dialogruta] Ändra inte huvudobjektets synlighet
- Nollställ överliggande objekt när huvudobjekt ändras

### Syfte

- Använd den trevligare K_PLUGIN_CLASS_WITH_JSON

### QQC2StyleBridge

- Rätta ursprunglig storlek för kombinationsrutor (fel 403736)
- Ställ in kombinationsmeddelande att ha fast läge (fel 403403)
- Ångra delvis 4f00b0cabc1230fdf
- Radbryt långa verktygstips (fel 396385)
- Kombinationsruta: Rätta förvald delegat
- Låtsas att musen hålls över medan kombinationsrutan är öppen
- Ställ in kombinationsrutans QStyleOptionState == On istället för Sunken för att motsvara qwidgets (fel 403153)
- Rätta kombinationsruta
- Rita alltid verktygstyps ovanpå allt annat
- Stöd verktygstips på ett musområde
- Tvinga inte textvisning för verktygsknapp

### Solid

- Bygg utan D-Bus på Android

### Sonnet

- Anropa inte den här koden om det bara finns mellanslag

### Syntaxfärgläggning

- Rätta vikområdets slut i regler med lookAhead=true
- AsciiDoc: Rätta färgläggning av include-direktiv
- Lägg till stöd för AsciiDoc
- Rätta fel som orsakade oändlig snurra vid färgläggning av Kconfig-filer
- Kontrollera oändliga kontextbyten
- Ruby: Rätta RegExp efter ": " och rätta/förbättra detektering av HEREDOC (fel 358273)
- Haskell: Gör = till en specialsymbol

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
