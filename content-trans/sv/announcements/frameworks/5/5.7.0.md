---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Allmänt

- Ett antal rättningar för att kompilera med det kommande Qt 5.5

### KActivities

- Rättade starta och stoppa aktiviteter
- Rättade förhandsgranskning av aktivitet som ibland visade fel skrivbordsunderlägg

### KArchive

- Skapa tillfälliga filer i den tillfälliga katalogen istället för arbetskatalogen

### KAuth

- Rättade generering av KAuth DBus-hjälptjänstfiler

### KCMUtils

- Rättade assert när DBus-sökvägar innehåller en '.'

### KCodecs

- Tillägg av stöd för CP949 i KCharsets

### KConfig

- kconf_update behandlar inte längre *.upd-fil från KDE SC 4. Tillägg av "Version=5" överst i upd-filen för uppdateringar som ska gälla Qt5/KF5-program
- Rättade KCoreConfigSkeleton när ett värde ändras och sparas emellan

### KConfigWidgets

- KRecentFilesAction: Rättade menyalternativens ordning (så den motsvarar ordningen i kdelibs4)

### KCoreAddons

- KAboutData: Anropa automatiskt addHelpOption och addVersionOption, av bekvämlighetsskäl och för konsekvens
- KAboutData: Återinförde "Använd http://bugs.kde.org för att rapportera fel." när ingen annan e-postadress eller webbadress är inställd
- KAutoSaveFile: allStaleFiles() fungerar nu som förväntat för lokala filer, rättade också staleFiles()
- KRandomSequence använder nu int internt och exponerar int-api för att inte vara tvetydigt för 64-bitar
- Mimetyp-definitioner: *.qmltypes- och *.qmlproject-filer har också Mime-typen text/x-qml
- KShell: Gör att quoteArgs citerar webbadresser med QChar::isSpace(), ovanliga mellanslagstecken hanterades inte riktigt
- KSharedDataCache: Rätta hur katalogen som innehåller cachen skapas (konverteringsfel)

### KDBusAddons

- Tillägg av hjälpmetoden KDEDModule::moduleForMessage för att kunna skriva mer kded-liknande demoner, såsom kiod

### KDeclarative

- Tillägg av en diagramkomponent
- Tillägg av överlagrad metod för Formats::formatDuration som använder int
- Nya egenskaper paintedWidth och paintedHeight tillagda för QPixmapItem och QImageItem
- Rättade uppritning av QImageItem och QPixmapItem

### Kded

- Tillägg av stöd för att läsa in kded-moduler med JSON-metadata

### KGlobalAccel

- Innehåller nu körningskomponenten, vilket gör det till ett lager 3-ramverk
- Gjorde att Windows-gränssnittet fungerar igen
- Aktiverade Mac-gränssnittet igen
- Rättade krasch i KGlobalAccel X11 körningsavstängning

### KI18n

- Markera resultat så att de måste varna vid felanvändning av programmeringsgränssnittet 
- Tillägg av byggsystemalternativet BUILD_WITH_QTSCRIPT för att tillåta en reducerad mängd funktioner för inbäddade system

### KInit

- OSX: Läs in rätt delade bibliotek vid körning
- Kompileringsrättningar för Mingw

### KIO

- Rättade krasch för job som länkar till KIOWidgets men bara använder QCoreApplication
- Rättade webbgenvägar för redigering
- Tillägg av alternativet KIOCORE_ONLY, för att bara kompilera KIOCore och dess hjälpprogram, men inte KIOWidgets eller KIOFileWidgets, och på så sätt starkt reducera nödvändiga beroenden
- Tillägg av klassen KFileCopyToMenu, som lägger till sammanhangsberoende menyerna Kopiera till och Flytta till
- SSL-aktiverade protokoll: Tillägg av stöd för protokollen TLSv1.1 och TLSv1.2, tog bort SSLv3
- Rättade negotiatedSslVersion och negotiatedSslVersionName så att de returnerar det faktiska förhandlade protokollet
- Använd inmatad webbadress för vyn vid klick på knappen som byter från webbnavigering tillbaka till länkstig
- Rättade att två förloppsrader eller dialogrutor visas för kopierings- och flyttningsjobb
- KIO använder nu sin egen demon, kiod, för tjänster utanför processen som tidigare kördes i kded, för att reducera beroenden, ersätter för närvarande bara kssld
- Rättade felet "Kunde inte skriva till &lt;sökväg&gt;" när kioexec utlöstes
- Rättade varningarna "QFileInfo::absolutePath: Konstruerad med tomt filnamn" vid användning av KFilePlacesModel

### KItemModels

- Rättade KRecursiveFilterProxyModel för Qt 5.5.0+, på grund av att QSortFilterProxyModel nu använder parametern roles i signalen dataChanged

### KNewStuff

- Läs alltid in xml-data igen från fjärrwebbadresser

### KNotifications

- Dokumentation: Kraven på filnamn för .notifyrc-filer nämns
- Rättade åtkomst av felaktiga pekare för KNotification
- Rättade läcka för knotifyconfig
- Installera saknad deklarationsfil för knotifyconfig

### KPackage

- Bytte namn på kpackagetool manualsida till kpackagetool5
- Rättade installation på skiftlägesokänsliga filsystem

### Kross

- Rättade Kross::MetaFunction så att det fungerar med Qt5:s metaobjektsystem

### KService

- Inkludera okända egenskaper vid konvertering av KPluginInfo från KService
- KPluginInfo: Rättade att egenskaper inte kopierades från KService::Ptr
- OS X: Prestandaförbättring för kbuildsycoca4 (hoppa över programpackar)

### KTextEditor

- Rättade högprecisionspanorering med tryckplatta
- Skicka inte documentUrlChanged vid återinläsning
- Förstör inte markörposition för återinläsning av dokument på rader med tabulatorer
- Lägg inte till eller ta bort vikning av första raden om den gjorts manuellt
- vimode: Kommandohistorik via piltangenter
- Försök inte skapa ett sammandrag när signalen KDirWatch::deleted() har tagits emot
- Prestanda: Ta bort globala initieringar

### KUnitConversion

- Rättade oändlig rekursion i Unit::setUnitMultiplier

### KWallet

- Detektera och konvertera automatiskt gamla ECB-plånböcker till CBC
- Rättade CBC-krypteringsalgoritmen
- Säkerställ att plånbokslistan uppdateras när en plånboksfil tas bort från disk
- Tog bort herrlösa &lt;/p&gt; i användarsynlig text

### KWidgetsAddons

- Använd kstyleextensions för att ange egna kontrollelement för att återgekcapacity-raden när det stöds, vilket låter den grafiska komponenten få en riktig stil
- Tillhandahåll ett åtkomligt namn för KLed

### KWindowSystem

- Rättade att NETRootInfo::setShowingDesktop(bool) inte fungerade på Openbox
- Tillägg av bekvämlighetsmetoden KWindowSystem::setShowingDesktop(bool)
- Rättningar i hantering av ikonformat
- Tillägg av metoden NETWinInfo::icccmIconPixmap tillhandahåller ikoners punktavbildning från egenskapen WM_HINTS
- Tillägg av överlagring för KWindowSystem::icon vilket reducerar tur och retur till X-servern
- Tillägg av stöd för _NET_WM_OPAQUE_REGION

### NetworkmanagerQt

- Skriv inte ut ett meddelande om ohanterad egenskap "AccessPoints"
- Tillägg av stöd för NetworkManager 1.0.0 (krävs inte)
- Rättade hantering av hemligheter för VpnSetting
- Tillägg av klassen GenericSetting för anslutningar som inte hanteras av NetworkManager
- Tillägg av egenskapen AutoconnectPriority i ConnectionSettings

#### Plasma ramverk

- Rättade att en felaktig sammanhangsberoende meny oriktigt öppnades vid mittenklick på Plasma meddelanderuta
- Utlös knappändring med mushjul
- Ändra aldrig storlek på en dialogruta så den blir större än bildskärmen
- Återställ paneler när miniprogram återställs
- Rättade snabbtangenter
- Återställ stöd för hint-apply-color-scheme
- Läs in inställningarna igen när plasmarc ändras
- ...

### Solid

- Tillägg av energyFull och energyFullDesign i Battery

### Ändringar av byggsystem (extra-cmake-modules)

- Ny modul ECMUninstallTarget för att skapa ett mål för avinstallering
- Låt KDECMakeSettings normalt importera ECMUninstallTarget
- KDEInstallDirs: Varna för att blanda relativa och absoluta installationssökvägar på kommandoraden
- Tillägg av modulen ECMAddAppIcon för att lägga till ikoner för körbara mål på Windows och Mac OS X
- Rättade varning CMP0053 med CMake 3.1
- Ta inte bort tilldelning av cache-variabler i KDEInstallDirs

### Frameworkintegration

- Rätta uppdatering av enkelklickinställningar vid körning
- Flera rättningar av systembrickans integrering
- Installera bara färgschema på grafiska toppnivåkomponenter (för att rätta QQuickWidgets)
- Uppdatera inställningar av XCursor på X11-plattformar

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
