---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE levererar Plasma arbetsrymder, program, och plattform 4.11.
title: KDE programvarusamling 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`KDE:s Plasma arbetsrymder 4.11` >}} <br />

14:e augusti 2013. KDE:s gemenskap är stolt över att tillkännage den senaste utgåvan av Plasma arbetsrymd, program och utvecklingsplattform som levererar nya funktioner och rättningar medan plattformen förbereds för framtida utveckling. Plasma arbetsrymder 4.11 får långtidsunderhåll medan gruppen fokuserar på den tekniska övergången till Ramverk 5. Därmed utgör detta den sista kombinerade utgåvan av arbetsrymder, program och plattform med samma versionsnummer.<br />

Den här utgåvan är dedicerad till minnet av <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a> en stor förkämpe av fri och öppen programvara från Indien. Atul ledde konferenserna Linux Bangalore och FOSS.IN sedan 2001, som båda var milstolpar på det indiska FOSS-området. KDE India startades vid den första FOSS konferensen December 2005. Många indiska bidragsgivare till KDE började vid dessa evenemang. Det var enbart på grund av Atuls uppmuntran som KDE:s projektdag vid FOSS.IN alltid var en enorm framgång. Atul lämnade oss den 3:e juni efter att ha kämpat med cancer. Må hans själ vila i frid. Vi är tacksamma för hans bidrag till en bättre värld.

Utgåvorna är alla översatta till 54 språk, och vi förväntar oss att flera språk läggs till i följande månadsvisa mindre felrättningsutgåvor av KDE. Dokumentationsgruppen uppdaterade 91 handböcker för denna utgåva.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma arbetsrymder 4.11 fortsätter förfina användarupplevelsen</a>

Plasma arbetsrymder växlar upp för långtidsunderhåll, och levererar ytterligare förbättringar av grundfunktionerna med en smidigare aktivitetsrad, smartare grafisk batterikomponent och förbättrad ljudmixer. Introduktionen av Kscreen ger arbetsrymderna intelligent hantering av flera bildskärmar, och storskaliga förbättringar av prestanda kombinerat med små justeringar av användbarhet leder till en trevligare övergripande upplevelse.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE-program 4.11 med enorma framsteg när det gäller personlig informationshantering och förbättringar överallt</a>

Utgåvan markerar omfattande förbättringar av KDE:s svit för personlig informationshantering, vilket ger mycket bättre prestanda och många nya funktioner. Kate förbättrar produktiviteten för Python- och Javascript-utvecklare med nya insticksprogram. Dolphin har blivit snabbare, och utbildningsprogrammen medför diverse nya funktioner.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE plattform 4.11 levererar bättre prestanda</a>

Den här utgåvan av KDE plattform 4.11 fortsätter att fokusera på stabilitet. Nya funktioner implementeras för den framtida utgåvan av KDE:s Ramverk 5.0, men för den stabila utgåvan lyckades vi klämma in optimeringar för vårt Nepomuk-ramverk.

<br />
Vid uppgradering, observera <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>versionsfakta</a>.<br />

## Sprid budskapet och se vad som händer: Markera med &quot;KDE&quot;

KDE uppmuntrar alla att sprida budskapet på den sociala webben. Skicka in historier till nyhetsplatser, använd kanaler som delicious, digg, reddit, twitter, identi.ca. Ladda upp skärmbilder till tjänster som Facebook, Flickr, ipernity och Picasa, och posta dem till lämpliga grupper. Skapa skärmfilmer och ladda upp dem till YouTube, Blip.tv och Vimeo. Markera gärna inlägg och uppladdat material med &quot;KDE&quot;. Det gör dem lätta att hitta och ger KDE:s marknadsföringsgrupp en möjlighet att analysera täckningen för utgåva 4.11 av KDE:s programvara.

## Utgivningsfester

Som vanligt organiserar medlemmar av KDE:s gemenskap utgivningsfester runt om i världen. Många har redan planerats, och flera kommer senare. Här finns <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>en lista av fester</a>. Alla är välkomna att delta! Det blir en kombination av intressanta deltagare och inspirerande samtal, samt mat och dryck. Det är ett utmärkt tillfälle att lära sig mer om vad som händer i KDE, börja delta, eller bara träffa andra användare och bidragsgivare.

Vi uppmuntrar alla att organisera sina egna fester. De är roliga att vara värd för, och är öppna för vem som helst. Titta på <a href='http://community.kde.org/Promo/Events/Release_Parties'>tips om hur man organiserar en fest</a>.

## Om dessa utgivningsmeddelanden

Dessa utgivningsmeddelanden har tagits fram av Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin och andra medlemmar i KDE:s marknadsföringsgrupp samt den bredare KDE-gemenskapen. De täcker höjdpunkterna av de många ändringar som gjorts i KDE:s programvara under de senaste sex månaderna.

#### Stöd KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

Det nya <a href='http://jointhegame.kde.org/'>programmet för stödmedlemmar</a> av KDE e.V. är nu öppet. För 25 &euro; per kvartal kan du försäkra dig om att KDE:s internationella gemenskap fortsätter att växa, och skapa fri programvara i världsklass.
