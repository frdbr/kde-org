---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE levererar KDE-program 15.12.3
layout: application
title: KDE levererar KDE-program 15.12.3
version: 15.12.3
---
15:e mars, 2016. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../15.12.0'>KDE-program 15.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 15 registrerade felrättningar omfattar förbättringar av bland annat kdepim, akonadi, ark, kblocks, kcalc, ktouch och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.18.
